/* Derived from echo.c in Core Utilities, derived from echo.c in Bash.
   echo -- display a line of text.
   Copyright (C) 1987-2016 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "echo-core.h"

/* Convert C from hexadecimal character to integer.  */
static int
hextobin (unsigned char c)
{
  switch (c)
    {
    default: return c - '0';
    case 'a': case 'A': return 10;
    case 'b': case 'B': return 11;
    case 'c': case 'C': return 12;
    case 'd': case 'D': return 13;
    case 'e': case 'E': return 14;
    case 'f': case 'F': return 15;
    }
}

/* Translates a C-like escape sequence in a null-terminated string.
   The following sequences are recognized:
   \"      double-quote
   \\      backslash
   \0      NUL (zero)
   \a      alert (BEL)
   \b      backspace
   \e      escape
   \f      form feed
   \n      new line
   \r      carriage return
   \t      horizontal tab
   \v      vertical tab
   \xHH    byte with hexadecimal value HH (1 to 2 digits) */

int
unescape (char *output, char *input)
{
	char const *s = input;
	unsigned char c;
	unsigned int d = 0;

	while ((c = *s++) != '\0')
		{
			if (c == '\\' && *s)
				{
					switch (c = *s++)
						{
						case '"': c = '"'; break;
						case '\\': break;
						case '0': c = '\0'; break;
						case 'a': c = '\a'; break;
						case 'b': c = '\b'; break;
						case 'e': c = '\e'; break;
						case 'f': c = '\f'; break;
						case 'n': c = '\n'; break;
						case 'r': c = '\r'; break;
						case 't': c = '\t'; break;
						case 'v': c = '\v'; break;
						case 'x':
							{
								unsigned char ch = *s;
								if (! isxdigit (ch))
									goto not_an_escape;
								s++;
								c = hextobin (ch);
								ch = *s;
								if (isxdigit (ch))
									{
										s++;
										c = c * 16 + hextobin (ch);
									}
							}
							break;
						not_an_escape:
						default:
							if (output) output[d] = '\\'; d++; break;
						}
				}
			if (output) output[d] = c; d++;
		}
	return d;
}

/* Translates a null-terminated string in a C-like escape sequence.
   Non-printable characters are translated as \xHH sequences */
int
escape (char *output, char *input, int length)
{
	char const *s = input;
	unsigned char c;
	unsigned int d = 0;

	while (s < input + length)
		{
			c = *s++;
			if (isprint(c))
				{
					if (output) output[d] = '\\';
					d++;
					switch (c)
						{
						case '"':
							if (output) output[d] = '"'; break;
						case '\\':
							if (output) output[d] = '\\'; break;
						case '\a':
							if (output) output[d] = 'a'; break;
						case '\b':
							if (output) output[d] = 'b'; break;
						case '\e':
							if (output) output[d] = 'e'; break;
						case '\f':
							if (output) output[d] = 'f'; break;
						case '\n':
							if (output) output[d] = 'n'; break;
						case '\r':
							if (output) output[d] = 'r'; break;
						case '\t':
							if (output) output[d] = 't'; break;
						case '\v':
							if (output) output[d] = 'v'; break;
						default:
							d--;
							if (output) output[d] = c; break;
						}
						d++;
				}
			else
			{
				if (output) sprintf(output + d, "\\x%02x", c); d += 4;
			}
		}
	if (output) output[d] = '\0'; d++;
	return d;
}
