/**
  ******************************************************************************
  * @file    canspy_debug.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   Debug module extension.
  *          This file provides all the functions needed to print data:
  *           + LED signaling
  *           + Formatting
	*           + Multi-output printing
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#include "canspy_uart.h"
#include "canspy_can.h"
#include "canspy_eth.h"
#include "canspy_sdcard.h"
#include "canspy_print.h"
#include "canspy_debug.h"

/** @brief  The default error message for memory allocation failures.
  */
char *CANSPY_MEMORY_ERROR_Alloc = "Failed to allocate memory";

/** @brief  The strings for debug levels.
  */
char *CANSPY_DEBUG_LEVEL_String[] = {"INFO", "WARN", "ERROR", "FATAL"};
	
/** @brief  Specifies when debug messages should be printed on devices.
  */
CANSPY_DEBUG_LEVEL CANSPY_DEBUG_SDCARD_Print = DBG_ERROR;
CANSPY_DEBUG_LEVEL CANSPY_DEBUG_UART_Print = DBG_INFO;
CANSPY_DEBUG_LEVEL CANSPY_DEBUG_ETH_Print = DBG_INFO;

/** @brief  Several counters for statistical purposes.
  */
uint32_t CANSPY_DEBUG_COUNT_Rx[] = {0, 0};
uint32_t CANSPY_DEBUG_COUNT_Miss[] = {0, 0};
uint32_t CANSPY_DEBUG_COUNT_Drop[] = {0, 0};
uint32_t CANSPY_DEBUG_COUNT_Altr[] = {0, 0};
uint32_t CANSPY_DEBUG_COUNT_Fwrd[] = {0, 0};
uint32_t CANSPY_DEBUG_COUNT_Tx[] = {0, 0};
uint32_t CANSPY_DEBUG_COUNT_Fail[] = {0, 0};
uint32_t CANSPY_DEBUG_COUNT_Inject[] = {0, 0};

/**
  * @brief  Process a debug message.
  * @param  level: the level of the debug message.
  * @param  handle: the destination handle.
  * @param  format: the format to print.
  * @param  args: the arguments.
  * @retval Exit Status
  */
int canspy_debug_process(CANSPY_DEBUG_LEVEL level, void *handle, const char* location, const char *format, va_list args){

	int len;
	char prefix;
	uint32_t frame_stamp;
	uint32_t frame_milli;
	
	len = vsnprintf(NULL, 0, format, args);
	
	if(len > 0){
		
		char buf[len + 1];

		len = vsnprintf(buf, len + 1, format, args);
		
		switch(level){
		
			case DBG_INFO:
				prefix = 'I';
				break;
			case DBG_WARN:
				prefix = 'W';
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
				break;
			case DBG_ERROR:
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);
				prefix = 'E';
				break;
			default:
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);
				prefix = 'F';
				break;
		}

		frame_milli = HAL_GetTick();
		frame_stamp = frame_milli / 1000;
		frame_milli = frame_milli - (frame_stamp * 1000);
		return (canspy_print_format(handle, "%i.%03i [%c] %s", frame_stamp, frame_milli, prefix, location ? location : "")) | (canspy_print_line(handle, buf, len));	
	}
	
	return EXIT_FAILURE;
}

/**
  * @brief  Print a debug message.
  * @param  level: the level of the debug message.
  * @param  handle: the destination handle.
  * @param  format: the format to print.
  * @param  ...:  the list of arguments.
  * @retval Exit Status
  */
int canspy_debug_print(CANSPY_DEBUG_LEVEL level, void *handle, const char *format, ...){

	va_list args;
	
	va_start(args, format);
	
	canspy_debug_process(level, handle, NULL, format, args);
	
	va_end(args);
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Broadcast a debug message.
  * @param  level: the level of the debug message.
  * @param  format: the format to print.
  * @param  ...:  the list of arguments.
  * @retval Exit Status
  */
int canspy_debug_broadcast(CANSPY_DEBUG_LEVEL level, const char *location, const char *format, ...){

	va_list args;
	
	va_start(args, format);
	
	if(CANSPY_DEVICE_ACTIVE(CANSPY_UART) && level >= CANSPY_DEBUG_UART_Print)
		canspy_debug_process(level, CANSPY_HANDLE_UART, location, format, args);
		
	if(CANSPY_DEVICE_ACTIVE(CANSPY_ETH) && level >= CANSPY_DEBUG_ETH_Print)
		canspy_debug_process(level, CANSPY_HANDLE_ETH, location, format, args);
		
	if(CANSPY_DEVICE_ACTIVE(CANSPY_SDCARD) && level >= CANSPY_DEBUG_SDCARD_Print && CANSPY_SDCARD_Log != NULL)
		return canspy_debug_process(level, CANSPY_SDCARD_Log, location, format, args);
	
	va_end(args);
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Create a debug message from an event.
  * @param  flag: the flag associated with the event. default rule / rule 1 matched
  * @param  handle: the destination handle.
  * @retval Exit Status
  */
int canspy_debug_event(CANSPY_SERVICE_FLAG flag, void *handle){
	
	if(flag == CAN1_RX || flag == CAN2_RX)
		return canspy_debug_print(DBG_INFO, handle, "Data received on CAN%d", flag);
#ifdef CANSPY_DEBUG_FILTER
	else if(flag == ALTR_CAN1 || flag == ALTR_CAN2){
		
		char *payload_string = canspy_debug_frame(NULL, 0, 0, flag, (void*)CANSPY_CAN_TxRx_Mess[ALTR_FLAG(flag)]);
		return canspy_debug_print(DBG_INFO, handle, "Data from CAN%d altered (rule:%i %s)", ALTR_FLAG(flag), match_rule_s_id[ALTR_FLAG(flag) - 1], payload_string);
	}
	else if(flag == DROP_CAN1 || flag == DROP_CAN2){
		
		if(dummy_drop[DROP_FLAG(flag) - 1])
			return canspy_debug_print(DBG_INFO, handle, "Data from CAN%d dummied (rule:%i)", DROP_FLAG(flag), match_rule_s_id[DROP_FLAG(flag) - 1]);
		else
			return canspy_debug_print(DBG_INFO, handle, "Data from CAN%d dropped (rule:%i)", DROP_FLAG(flag), match_rule_s_id[DROP_FLAG(flag) - 1]);
	}
	else if(flag == FWRD_CAN1 || flag == FWRD_CAN2){
		
		if(filtered_forwarding[FWRD_FLAG(flag) - 1])
			return canspy_debug_print(DBG_INFO, handle, "Data forwarded to CAN%d (rule:%i)", FWRD_FLAG(flag), match_rule_d_id[FWRD_FLAG(flag) - 1]);
		else
			return canspy_debug_print(DBG_INFO, handle, "Data automatically forwarded to CAN%d", FWRD_FLAG(flag));
	}
#endif
	else if(flag == INJ_ETH && CANSPY_INJ_Services[CANSPY_INJ_ETH_If]->started && CANSPY_DEVICE_ACTIVE(CANSPY_INJ_Services[CANSPY_INJ_ETH_If]->dev_ptr->dev_id))
		return canspy_debug_print(DBG_INFO, handle, "Data injected from ETH to CAN%i", CANSPY_INJ_ETH_If);
	else if(flag == INJ_UART && CANSPY_INJ_Services[CANSPY_INJ_UART_If]->started && CANSPY_DEVICE_ACTIVE(CANSPY_INJ_Services[CANSPY_INJ_UART_If]->dev_ptr->dev_id))
		return canspy_debug_print(DBG_INFO, handle, "Data injected from UART to CAN%i", CANSPY_INJ_UART_If);
	else if(flag == INJ_SVC && CANSPY_INJ_Services[CANSPY_INJ_SVC_If]->started && CANSPY_DEVICE_ACTIVE(CANSPY_INJ_Services[CANSPY_INJ_SVC_If]->dev_ptr->dev_id))
		return canspy_debug_print(DBG_INFO, handle, "Data injected from a service to CAN%i", CANSPY_INJ_UART_If);
	
	return EXIT_FAILURE;
}

/**
  * @brief  Create a debug message from a frame.
  * @param  handle: the destination handle.
  * @param  templ: first temporal value.
  * @param  temp2: second temporal value.
  * @param  can_if: the can interface number.
  * @param  frame: the frame to process.
  * @retval A pointer to the stringified payload
  */
char *canspy_debug_frame(void *handle, uint32_t temp1, uint32_t temp2, uint8_t can_if, CanRxMsgTypeDef *frame){
	
	int print_len;
	static char payload_string[(sizeof(frame->Data) + 1) * 4 + 8];

	if(handle != NULL)
		canspy_print_format(handle, "%i.%03i Can:%d Id:%d Length:%d ", temp1, temp2, can_if, frame->StdId, frame->DLC);
		
	if(frame->RTR == CAN_RTR_REMOTE)
		canspy_print_format_buffer(payload_string, sizeof(payload_string), "RTR");
	else{
		
		for(int i = print_len = 0; i < frame->DLC; i++)
			print_len += canspy_print_format_buffer(payload_string + print_len, sizeof(payload_string) - print_len, "%s\\x%02x", (i == 0 ? "DATA:" : ""), frame->Data[i]);
	}
	
	if(handle != NULL)
		canspy_print_line(CANSPY_HANDLE_UART, payload_string, 0);
	
	return payload_string;
}

/**
  * @brief  Create a debug message from counters.
  * @param  handle: the destination handle.
  * @param  can_if: the can interface number.
  * @param  reset: specifies whether to reset the counters.
  * @retval Exit Status
  */
int canspy_debug_stats(void *handle, uint8_t can_if, bool reset){
	
	canspy_print_format_line(handle, "CAN%i", can_if);
	canspy_print_format_line(handle, "\tRx: %i", CANSPY_DEBUG_COUNT_Rx[can_if - 1]);
	canspy_print_format_line(handle, "\tMiss: %i", CANSPY_DEBUG_COUNT_Miss[can_if - 1]);
	canspy_print_format_line(handle, "\tDrop: %i", CANSPY_DEBUG_COUNT_Drop[can_if - 1]);
	canspy_print_format_line(handle, "\tAltr: %i", CANSPY_DEBUG_COUNT_Altr[can_if - 1]);
	canspy_print_format_line(handle, "\tFwrd: %i", CANSPY_DEBUG_COUNT_Fwrd[can_if - 1]);
	canspy_print_format_line(handle, "\tTx: %i", CANSPY_DEBUG_COUNT_Tx[can_if - 1]);
	canspy_print_format_line(handle, "\tFail: %i", CANSPY_DEBUG_COUNT_Fail[can_if - 1]);
	canspy_print_format_line(handle, "\tInject: %i", CANSPY_DEBUG_COUNT_Inject[can_if - 1]);
	
	if(reset){
		
		CANSPY_DEBUG_COUNT_Rx[can_if - 1] = 0;
		CANSPY_DEBUG_COUNT_Miss[can_if - 1] = 0;
		CANSPY_DEBUG_COUNT_Drop[can_if - 1] = 0;
		CANSPY_DEBUG_COUNT_Altr[can_if - 1] = 0;
		CANSPY_DEBUG_COUNT_Fwrd[can_if - 1] = 0;
		CANSPY_DEBUG_COUNT_Tx[can_if - 1] = 0;
		CANSPY_DEBUG_COUNT_Fail[can_if - 1] = 0;
		CANSPY_DEBUG_COUNT_Inject[can_if - 1] = 0;
	}
		
	return EXIT_SUCCESS;
}
