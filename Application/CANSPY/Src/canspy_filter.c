/**
  ******************************************************************************
  * @file    canspy_filter.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   Filter module core.
  *          This file provides all the functions to manage filters:
  *           + Initializing
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#include "canspy_filter.h"
#include "canspy_debug.h"
#include "memmem.h"
#include "slre.h"
#include "echo-core.h"

/** @brief  Strings associated with the corresponding enumerated values.
  */
char CANSPY_FILTER_INT_Char[] = {'<', '>', '=', '!', '*'};
char CANSPY_FILTER_ANY_String[] = "ANY";
char *CANSPY_FILTER_CAN_String[] = {CANSPY_FILTER_ANY_String, "CAN1", "CAN2", "ALL"};
char *CANSPY_FILTER_RTR_String[] = {"DATA", CANSPY_FILTER_ANY_String, "RTR"};
char *CANSPY_FILTER_BUF_String[] = {CANSPY_FILTER_ANY_String, "BEG", "END", "EQU", "CON", "REG"};
char *CANSPY_FILTER_ACT_String[] = {"DROP", "ALTR", "FWRD", "ANY"};

/**
  * @brief  Register a new rule.
  * @param  fil_ptr: a pointer to the filter structure.
  * @param  out_rul: a double pointer to new rule.
  * @param  argc/argv: the parameters using command-like inputs.
  * @retval Exit Status
  */  
int canspy_filter_register(CANSPY_FILTER_DESC *fil_ptr, CANSPY_FILTER_RULE **out_rul, int argc, char *argv[]){
	
	int i, len;
	CANSPY_FILTER_RULE new_rule;
	CANSPY_FILTER_RULE *rul_list;
	
	if(argc < 6)
		return EXIT_FAILURE;
	
	if(fil_ptr->rul_size > (sizeof(fil_ptr->rul_size) * 255) - 1)
		return EXIT_FAILURE;
	
	/* Processing the frame source */
	if(strcasecmp(CANSPY_FILTER_CAN_String[CAN_ANY], argv[0]) == 0)
		new_rule.can_if = CAN_ANY;
	else if(strcasecmp(CANSPY_FILTER_CAN_String[CAN_IF1], argv[0]) == 0)
		new_rule.can_if = CAN_IF1;
	else if(strcasecmp(CANSPY_FILTER_CAN_String[CAN_IF2], argv[0]) == 0)
		new_rule.can_if = CAN_IF2;
	else
		return EXIT_FAILURE;

	/* Processing the frame id */
	if(strcasecmp(CANSPY_FILTER_ANY_String, argv[1]) == 0){

		new_rule.can_id = 0;
		new_rule.cmp_id = INT_ANY;
	}
	else if((new_rule.can_id = strtol(argv[1] + 1, NULL, 0)) != 0){
		
		if(argv[1][0] == CANSPY_FILTER_INT_Char[INT_LOW])
			new_rule.cmp_id = INT_LOW;
		else if(argv[1][0] == CANSPY_FILTER_INT_Char[INT_HIG])
			new_rule.cmp_id = INT_HIG;
		else if(argv[1][0] == CANSPY_FILTER_INT_Char[INT_EQU])
			new_rule.cmp_id = INT_EQU;
		else if(argv[1][0] == CANSPY_FILTER_INT_Char[INT_DIF])
			new_rule.cmp_id = INT_DIF;
		else
			return EXIT_FAILURE;
	}
	else
		return EXIT_FAILURE;
	
	/* Processing the frame type */
	if(strcasecmp(CANSPY_FILTER_RTR_String[RTR_ANY], argv[2]) == 0)
		new_rule.can_rtr = RTR_ANY;
	else if(strcasecmp(CANSPY_FILTER_RTR_String[RTR_REM], argv[2]) == 0)
		new_rule.can_rtr = RTR_REM;
	else if(strcasecmp(CANSPY_FILTER_RTR_String[RTR_DAT], argv[2]) == 0)
		new_rule.can_rtr = RTR_DAT;
	else
		return EXIT_FAILURE;
	
	/* Processing the frame length */
	if(strcasecmp(CANSPY_FILTER_ANY_String, argv[3]) == 0){

		new_rule.can_dlc = 0;
		new_rule.cmp_dlc = INT_ANY;
	}
	else if((new_rule.can_dlc = strtol(argv[3] + 1, NULL, 0)) != 0){
		
		if(argv[3][0] == CANSPY_FILTER_INT_Char[INT_LOW])
			new_rule.cmp_dlc = INT_LOW;
		else if(argv[3][0] == CANSPY_FILTER_INT_Char[INT_HIG])
			new_rule.cmp_dlc = INT_HIG;
		else if(argv[3][0] == CANSPY_FILTER_INT_Char[INT_EQU])
			new_rule.cmp_dlc = INT_EQU;
		else if(argv[3][0] == CANSPY_FILTER_INT_Char[INT_DIF])
			new_rule.cmp_dlc = INT_DIF;
		else
			return EXIT_FAILURE;
	}
	else
		return EXIT_FAILURE;
	
	/* Processing the frame data */
	if(strcasecmp(CANSPY_FILTER_BUF_String[BUF_ANY], argv[4]) == 0){

		new_rule.can_data = NULL;
		new_rule.len_data = 0;
		new_rule.cmp_data = BUF_ANY;
	}
	else{
		
		if((len = strlen(argv[4])) < 5)
			return EXIT_FAILURE;
		
		if(argv[4][3] != ':')
			return EXIT_FAILURE;
		
		argv[4][3] = '\0';
		
		if(strcasecmp(CANSPY_FILTER_BUF_String[BUF_BEG], argv[4]) == 0)
			new_rule.cmp_data = BUF_BEG;
		else if(strcasecmp(CANSPY_FILTER_BUF_String[BUF_END], argv[4]) == 0)
			new_rule.cmp_data = BUF_END;
		else if(strcasecmp(CANSPY_FILTER_BUF_String[BUF_EQU], argv[4]) == 0)
			new_rule.cmp_data = BUF_EQU;
		else if(strcasecmp(CANSPY_FILTER_BUF_String[BUF_CON], argv[4]) == 0)
			new_rule.cmp_data = BUF_CON;
		else if(strcasecmp(CANSPY_FILTER_BUF_String[BUF_REG], argv[4]) == 0)
			new_rule.cmp_data = BUF_REG;
		else
			return EXIT_FAILURE;
		
		if(new_rule.cmp_data == BUF_REG){
			
			new_rule.can_data = canspy_print_format_alloc("%s", argv[4] + 4);
			new_rule.len_data = len - 4;
		}
		else{
			
			if((len = unescape(NULL, argv[4] + 4)) == 0)
				return EXIT_SUCCESS;
			
			if((new_rule.can_data = malloc(len)) == NULL){

				CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
				return EXIT_FAILURE;
			}
			
			unescape(new_rule.can_data, argv[4] + 4);
			new_rule.len_data = len;
		}
	}
	
	/* Processing the associated action */
	if(strcasecmp(CANSPY_FILTER_ACT_String[ACT_DROP], argv[5]) == 0)
		new_rule.can_act = ACT_DROP;
	else if(strcasecmp(CANSPY_FILTER_ACT_String[ACT_ALTR], argv[5]) == 0)
		new_rule.can_act = ACT_ALTR;
	else if(strcasecmp(CANSPY_FILTER_ACT_String[ACT_FWRD], argv[5]) == 0)
		new_rule.can_act = ACT_FWRD;
	else{
		
		CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
		canspy_filter_free(&new_rule);
		return EXIT_FAILURE;
	}
	
	/* Initializing CAP pointers to NULL and length to 0 in case of EXIT_FAILURE */
	new_rule.act_cap = NULL;
	new_rule.can_cap = NULL;
	new_rule.cap_len = 0;
	
	/* Processing the altering data */
	if(argc > 6){
		
		new_rule.cap_len = argc - 6;
		new_rule.act_cap = malloc(new_rule.cap_len * sizeof(struct slre_cap));
		new_rule.can_cap = malloc(new_rule.cap_len * sizeof(struct slre_cap));
		
		if(new_rule.act_cap == NULL || new_rule.can_cap == NULL){
			
			CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
			canspy_filter_free(&new_rule);
			return EXIT_FAILURE;
		}
		
		for(i = 0; i < new_rule.cap_len; i++){

			if((new_rule.act_cap[i].len = unescape(NULL, argv[i + 6])) == 0){
				
				CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
				canspy_filter_free(&new_rule);
				return EXIT_SUCCESS;
			}
			
			if((new_rule.act_cap[i].ptr = malloc(new_rule.act_cap[i].len)) == NULL){
				
				CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
				canspy_filter_free(&new_rule);
				return EXIT_SUCCESS;
			}
			
			unescape((char *)new_rule.act_cap[i].ptr, argv[i + 6]);
		}
	}
	
	/* Adding the new rule to the specified filter */
	if((rul_list = realloc(fil_ptr->rul_list, (fil_ptr->rul_size + 1) * sizeof(CANSPY_FILTER_RULE))) == NULL){
		
		CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
		canspy_filter_free(&new_rule);
		return EXIT_FAILURE;
	}
	
	fil_ptr->rul_list = rul_list;
	rul_list[fil_ptr->rul_size] = new_rule;
	rul_list[fil_ptr->rul_size].rul_id = fil_ptr->rul_size;
	fil_ptr->rul_size++;
	
	if(out_rul)
		*out_rul = &(rul_list[fil_ptr->rul_size - 1]);
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Print a specified rule.
  * @param  handle: the destination handle.
  * @param  rul_ptr: the rule to print.
  * @retval Exit Status;
  */
int canspy_filter_print(void *handle, CANSPY_FILTER_RULE *rul_ptr){
	
	int i, tmp_len;
	char *tmp_str;
	
	/* Printing the rule id */
	canspy_print_format(handle, "RULE:%i", rul_ptr->rul_id + 1);
	
	/* Printing the frame source */
	canspy_print_format(handle, " %s", CANSPY_FILTER_CAN_String[rul_ptr->can_if]);
		
	/* Printing the frame id */
	if(rul_ptr->cmp_id == INT_ANY)
		canspy_print_format(handle, " %s", CANSPY_FILTER_ANY_String);
	else
		canspy_print_format(handle, " %c%i", CANSPY_FILTER_INT_Char[rul_ptr->cmp_id], rul_ptr->can_id);
	
	/* Printing the frame type */
	canspy_print_format(handle, " %s", CANSPY_FILTER_RTR_String[rul_ptr->can_rtr]);
	
	/* Printing the frame length */
	if(rul_ptr->cmp_dlc == INT_ANY)
		canspy_print_format(handle, " %s", CANSPY_FILTER_ANY_String);
	else
		canspy_print_format(handle, " %c%i", CANSPY_FILTER_INT_Char[rul_ptr->cmp_dlc], rul_ptr->can_dlc);
	
	/* Printing the frame data */
	if(rul_ptr->cmp_data == BUF_ANY)
		canspy_print_format(handle, " %s", CANSPY_FILTER_BUF_String[rul_ptr->cmp_data]);
	else if(rul_ptr->cmp_data == BUF_REG)
		canspy_print_format(handle, " %s:\"%s\"", CANSPY_FILTER_BUF_String[rul_ptr->cmp_data], rul_ptr->can_data);
	else{
		
		tmp_len = escape(NULL, rul_ptr->can_data, rul_ptr->len_data);
		if((tmp_str = malloc(tmp_len)) == NULL){
			
			CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
			return EXIT_FAILURE;
		}
		
		escape(tmp_str, rul_ptr->can_data, rul_ptr->len_data);
		canspy_print_format(handle, " %s:\"%s\"", CANSPY_FILTER_BUF_String[rul_ptr->cmp_data], tmp_str);
		free(tmp_str);
	}
	
	/* Printing the associated action */
	canspy_print_format(handle, " %s", CANSPY_FILTER_ACT_String[rul_ptr->can_act]);
	
	/* Printing the altering data */
	for(i = 0; i < rul_ptr->cap_len; i++){
		
		tmp_len = escape(NULL, (char *)rul_ptr->act_cap[i].ptr, rul_ptr->act_cap[i].len);
		if((tmp_str = malloc(tmp_len)) == NULL){
			
			CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
			return EXIT_FAILURE;
		}
		
		escape(tmp_str, (char *)rul_ptr->act_cap[i].ptr, rul_ptr->act_cap[i].len);
		canspy_print_format(handle, " \"%s\"", tmp_str);
		free(tmp_str);
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Free the memory allocated to a specified rule.
  * @param  rul_ptr: the rule to free.
  * @retval Exit Status;
  */
int canspy_filter_free(CANSPY_FILTER_RULE *rul_ptr){
	
	int i;
	
	if(!rul_ptr)
		return EXIT_FAILURE;
	
	if(rul_ptr->can_data)
		free(rul_ptr->can_data);
	
	if(rul_ptr->act_cap){
		
		for(i = 0; i < rul_ptr->cap_len; i++){
			
			if(rul_ptr->act_cap[i].ptr)
				free((void *)(rul_ptr->act_cap[i].ptr));
		}
		
		free(rul_ptr->act_cap);
	}
			
	if(rul_ptr->can_cap)
		free(rul_ptr->can_cap);

	return EXIT_SUCCESS;
}

/**
  * @brief  Remove a registered a rule.
  * @param  fil_ptr: a pointer to the filter structure.
  * @param  rul_id: the id of the rule to remove.
  * @retval Exit Status
  */
int canspy_filter_remove(CANSPY_FILTER_DESC *fil_ptr, uint8_t rul_id){
	
	int i;
	
	for(i = 0; i < fil_ptr->rul_size; i++){
		
		if(fil_ptr->rul_list[i].rul_id == rul_id)
			break;
	}
	
	if(i == fil_ptr->rul_size)
		return EXIT_FAILURE;
	
	canspy_filter_free(&fil_ptr->rul_list[i]);
	
	for(i = i; i < fil_ptr->rul_size - 1; i++){
		
		fil_ptr->rul_list[i + 1].rul_id = fil_ptr->rul_list[i].rul_id;
		fil_ptr->rul_list[i] = fil_ptr->rul_list[i + 1];
	}
		
	fil_ptr->rul_size--;
	
	if(fil_ptr->rul_size == 0){

		free(fil_ptr->rul_list);
		fil_ptr->rul_list = NULL;
	}
	else
		fil_ptr->rul_list = realloc(fil_ptr->rul_list, fil_ptr->rul_size * sizeof(CANSPY_FILTER_RULE));
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Walk through all rules of a specified filter.
  * @param  fil_ptr: the specified filter.
  * @param  cur_rul: the current rule or NULL to start.
  * @retval The next rule
  */
CANSPY_FILTER_RULE *canspy_filter_walk(CANSPY_FILTER_DESC *fil_ptr, CANSPY_FILTER_RULE *cur_rul){

	if(fil_ptr == NULL || fil_ptr->rul_size <= 0)
		return NULL;
	
	if(cur_rul == NULL)
		return &(fil_ptr->rul_list[0]);
	
	if(cur_rul->rul_id >= fil_ptr->rul_size - 1)
		return NULL;
	
	return &(fil_ptr->rul_list[cur_rul->rul_id + 1]);
}

/**
  * @brief  Test if a CAN frame matches a specified rule.
  * @param  can_if: the source of the CAN frame.
  * @param  rul_ptr: the specified rule.
  * @param  pRxMess: the CAN frame to test.
  * @retval The number of matched bytes
  */
int canspy_filter_match(int can_if, CANSPY_FILTER_RULE *rul_ptr, CanRxMsgTypeDef *pRxMess){
	
	void *p;
	void **pp;
	uint32_t can_id;
	
	/* Testing the frame source */
	if(rul_ptr->can_if != CAN_ANY && rul_ptr->can_if != can_if)
		return EXIT_FAILURE;
	
	/* Testing the frame id */
	if(rul_ptr->cmp_id != INT_ANY){
		
		if(pRxMess->IDE == CAN_ID_EXT)
			can_id = pRxMess->ExtId;
		else
			can_id = pRxMess->StdId;
		
		if(rul_ptr->cmp_id == INT_EQU && rul_ptr->can_id != can_id)
			return EXIT_FAILURE;
		else if(rul_ptr->cmp_id == INT_DIF && rul_ptr->can_id == can_id)
			return EXIT_FAILURE;
		else if(rul_ptr->cmp_id == INT_LOW && rul_ptr->can_id <= can_id)
			return EXIT_FAILURE;
		else if(rul_ptr->cmp_id == INT_HIG && rul_ptr->can_id >= can_id)
			return EXIT_FAILURE;
	}
	
	/* Testing the frame type */
	if(rul_ptr->can_rtr != RTR_ANY && rul_ptr->can_rtr != pRxMess->RTR)
		return EXIT_FAILURE;
	
	/* Testing the frame length */
	if(rul_ptr->cmp_dlc != INT_ANY){
		
		if(rul_ptr->cmp_dlc == INT_EQU && rul_ptr->can_dlc != pRxMess->DLC)
			return EXIT_FAILURE;
		else if(rul_ptr->cmp_dlc == INT_DIF && rul_ptr->can_dlc == pRxMess->DLC)
			return EXIT_FAILURE;
		else if(rul_ptr->cmp_dlc == INT_LOW && rul_ptr->can_dlc <= pRxMess->DLC)
			return EXIT_FAILURE;
		else if(rul_ptr->cmp_dlc == INT_HIG && rul_ptr->can_dlc >= pRxMess->DLC)
			return EXIT_FAILURE;
	}
	
	/* Testing the frame data */
	if(rul_ptr->cmp_data != BUF_ANY){
		
		/* There is no data to check in a RTR frame */
		if(pRxMess->RTR == CAN_RTR_REMOTE)
			return EXIT_FAILURE;

		if(rul_ptr->cap_len == 0)
			pp = &p;
		else
			pp = (void **)&(rul_ptr->can_cap[0].ptr);
		
		if(rul_ptr->cmp_data == BUF_BEG
		&& (rul_ptr->len_data > pRxMess->DLC || memcmp(rul_ptr->can_data, pRxMess->Data, rul_ptr->len_data) != 0))
			return EXIT_FAILURE;
		else if(rul_ptr->cmp_data == BUF_END
		&& (rul_ptr->len_data > pRxMess->DLC || memcmp(rul_ptr->can_data, pRxMess->Data + (pRxMess->DLC - rul_ptr->len_data), rul_ptr->len_data) != 0))
			return EXIT_FAILURE;
		else if(rul_ptr->cmp_data == BUF_EQU
		&& (rul_ptr->len_data != pRxMess->DLC || memcmp(rul_ptr->can_data, pRxMess->Data, rul_ptr->len_data) != 0))
			return EXIT_FAILURE;
		else if(rul_ptr->cmp_data == BUF_CON
		&& (rul_ptr->len_data > pRxMess->DLC || (*pp = memmem(pRxMess->Data, pRxMess->DLC, rul_ptr->can_data, rul_ptr->len_data)) == NULL))
			return EXIT_FAILURE;
		else if(rul_ptr->cmp_data == BUF_REG
		&& (slre_match(rul_ptr->can_data, (char *)(pRxMess->Data), pRxMess->DLC, rul_ptr->can_cap, rul_ptr->cap_len, 0) == 0))
			return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Alter a CAN frame using the provided rule.
  * @param  rul_ptr: the specified rule.
  * @param  CAN_Tx_Mess: the destination CAN frame.
  * @param  CAN_Rx_Mess: the source CAN frame.
  * @retval Exit Status (SUCCESS if altered, FAILURE if copied)
  */
int canspy_filter_alter(CANSPY_FILTER_RULE *rul_ptr, CanTxMsgTypeDef *pCAN_Tx_Mess, CanRxMsgTypeDef *pCAN_Rx_Mess){
	
	int i, j, k;
	
	pCAN_Tx_Mess->StdId = pCAN_Rx_Mess->StdId;
	pCAN_Tx_Mess->ExtId = pCAN_Rx_Mess->ExtId;
	pCAN_Tx_Mess->IDE = pCAN_Rx_Mess->IDE;
	pCAN_Tx_Mess->RTR = pCAN_Rx_Mess->RTR;
	
	if(rul_ptr->cap_len == 0 || rul_ptr->cmp_data == BUF_ANY){
		
		memcpy(pCAN_Tx_Mess->Data, pCAN_Rx_Mess->Data, pCAN_Rx_Mess->DLC);
		pCAN_Tx_Mess->DLC = pCAN_Rx_Mess->DLC;
		return EXIT_FAILURE;
	}
	else if(rul_ptr->cmp_data == BUF_BEG){

		i = j = 0;
		while(i < sizeof(pCAN_Tx_Mess->Data) && j < rul_ptr->act_cap[0].len)
			pCAN_Tx_Mess->Data[i++] = rul_ptr->act_cap[0].ptr[j++];
		j = rul_ptr->len_data;
		while(i < sizeof(pCAN_Tx_Mess->Data) && j < pCAN_Rx_Mess->DLC)
			pCAN_Tx_Mess->Data[i++] = pCAN_Rx_Mess->Data[j++];
		pCAN_Tx_Mess->DLC = i;
	}
	else if(rul_ptr->cmp_data == BUF_END){

		i = j = 0;
		while(i < sizeof(pCAN_Tx_Mess->Data) && j < pCAN_Rx_Mess->DLC - rul_ptr->len_data)
			pCAN_Tx_Mess->Data[i++] = pCAN_Rx_Mess->Data[j++];
		j = 0;
		while(i < sizeof(pCAN_Tx_Mess->Data) && j < rul_ptr->act_cap[0].len)
			pCAN_Tx_Mess->Data[i++] = rul_ptr->act_cap[0].ptr[j++];
		pCAN_Tx_Mess->DLC = i;
	}
	else if(rul_ptr->cmp_data == BUF_EQU){

		i = j = 0;
		while(i < sizeof(pCAN_Tx_Mess->Data) && j < rul_ptr->act_cap[0].len)
			pCAN_Tx_Mess->Data[i++] = rul_ptr->act_cap[0].ptr[j++];
		pCAN_Tx_Mess->DLC = i;
	}
	else if(rul_ptr->cmp_data == BUF_CON){

		i = j = 0;
		while(i < sizeof(pCAN_Tx_Mess->Data) && j < ((unsigned int)(rul_ptr->can_cap[0].ptr) - (unsigned int)(pCAN_Rx_Mess->Data)))
			pCAN_Tx_Mess->Data[i++] = pCAN_Rx_Mess->Data[j++];
		j = 0;
		while(i < sizeof(pCAN_Tx_Mess->Data) && j < rul_ptr->act_cap[0].len)
			pCAN_Tx_Mess->Data[i++] = rul_ptr->act_cap[0].ptr[j++];
		j = (unsigned int)(rul_ptr->can_cap[0].ptr + rul_ptr->len_data) - (unsigned int)(pCAN_Rx_Mess->Data);
		while(i < sizeof(pCAN_Tx_Mess->Data) && j < pCAN_Rx_Mess->DLC)
			pCAN_Tx_Mess->Data[i++] = pCAN_Rx_Mess->Data[j++];
		pCAN_Tx_Mess->DLC = i;
	}
	else if(rul_ptr->cmp_data == BUF_REG){
		
		i = k = 0;
		while(i < sizeof(pCAN_Tx_Mess->Data) && k < rul_ptr->cap_len){
			
			if(k == 0){
				j = 0; //(k == 0 ? 0 : ((unsigned int)(rul_ptr->can_cap[k - 1].ptr + rul_ptr->can_cap[k - 1].len) - (unsigned int)(pCAN_Rx_Mess->Data)));
				while(i < sizeof(pCAN_Tx_Mess->Data) && j < ((unsigned int)(rul_ptr->can_cap[k].ptr) - (unsigned int)(pCAN_Rx_Mess->Data)))
					pCAN_Tx_Mess->Data[i++] = pCAN_Rx_Mess->Data[j++];
			}
			j = 0;
			while(i < sizeof(pCAN_Tx_Mess->Data) && j < rul_ptr->act_cap[k].len)
				pCAN_Tx_Mess->Data[i++] = rul_ptr->act_cap[k].ptr[j++];
			j = ((unsigned int)(rul_ptr->can_cap[k].ptr + rul_ptr->can_cap[k].len) - (unsigned int)(pCAN_Rx_Mess->Data));
			while(i < sizeof(pCAN_Tx_Mess->Data) && j < (k == rul_ptr->cap_len - 1 ? pCAN_Rx_Mess->DLC : ((unsigned int)(rul_ptr->can_cap[k + 1].ptr) - (unsigned int)(pCAN_Rx_Mess->Data))))
				pCAN_Tx_Mess->Data[i++] = pCAN_Rx_Mess->Data[j++];

			k++;
		}
		pCAN_Tx_Mess->DLC = i;
	}
	
	return EXIT_SUCCESS;
}
