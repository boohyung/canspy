/**
  ******************************************************************************
  * @file    canspy_print.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   Print module extension.
  *          This file provides all the functions needed to print data:
  *           + Output selection
  *           + Formatting
	*           + Line-breaking
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#include "canspy_print.h"
#include "canspy_uart.h"
#include "canspy_eth.h"
#include "canspy_sdcard.h"
#include "canspy_debug.h"

/** @brief  The default padding value.
  */
uint8_t CANSPY_PRINT_PAD_Option = 1;
uint8_t CANSPY_PRINT_PAD_Device = 2;
uint8_t CANSPY_PRINT_PAD_Service = 1;
uint8_t CANSPY_PRINT_PAD_Value = 1;
uint8_t CANSPY_PRINT_PAD_Range = 2;

/** @brief  The maximum length of internal strings.
  */
int CANSPY_PRINT_MAX_Option = 0;
int CANSPY_PRINT_MAX_Device = 0;
int CANSPY_PRINT_MAX_Service = 0;
int CANSPY_PRINT_MAX_Value = 0;
int CANSPY_PRINT_MAX_Range = 0;
int CANSPY_PRINT_MAX_Command = 0;

/**
  * @brief  Print a buffer.
  * @param  handle: the destination handle.
  * @param  buffer: the buffer to print.
  * @param  length: the length of the buffer.
  * @retval Exit Status
  */
int canspy_print_buffer(void *handle, void *buffer, int len){
	
	if(len <= 0){
		
		CANSPY_DEBUG_ERROR_0(ERROR, "Invalid buffer length");
		return EXIT_FAILURE;
	}
	
	if((handle == CANSPY_HANDLE_UART) && CANSPY_DEVICE_ACTIVE(CANSPY_UART)){

		canspy_uart_queue(buffer, len);
	}
	else if(CANSPY_DEVICE_ACTIVE(CANSPY_ETH) && ((handle == CANSPY_HANDLE_ETH)
	|| ((handle == CANSPY_HANDLE_UART) && CANSPY_ETH_CMD_Output && !CANSPY_DEVICE_ACTIVE(CANSPY_UART)))){
		
		canspy_eth_send(ETH_Tx_Buf[0], buffer, len, CANSPY_ETH_Mac[ETH_IF], CANSPY_ETH_Mac[ETH_BROADCAST], CANSPY_ETH_Type[ETH_IF], false);
	}
	else if(CANSPY_DEVICE_ACTIVE(CANSPY_SDCARD) && (handle != NULL)){
		
		canspy_sdcard_write(handle, buffer, len, false);
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Print a character if printable (a dot otherwise).
  * @param  handle: the destination handle.
  * @param  string: the char to print.
  * @retval Exit Status
  */
int canspy_print_printable(void *handle, char single){
	
	if(single < 32 || single >= 127)
		single = '.';

	return canspy_print_buffer(handle, &single, sizeof(single));
}

/**
  * @brief  Print a string.
  * @param  handle: the destination handle.
  * @param  string: the string to print.
  * @param  len: the length of the string (optional).
  * @retval Exit Status
  */
int canspy_print_string(void *handle, const char *string, int len){
	
	if(len == 0)
		len = strlen(string);

	return canspy_print_buffer(handle, (char *)string, len);
}

/**
  * @brief  Print a line.
  * @param  handle: the destination handle.
  * @param  line: the line to print.
  * @param  len: the length of the string (optional).
  * @retval Exit Status
  */
int canspy_print_line(void *handle, const char *string, int len){
	
	return (canspy_print_string(handle, string, len)) | (canspy_print_buffer(handle, CANSPY_PRINT_BREAK, sizeof(CANSPY_PRINT_BREAK) - 1));
}

/**
  * @brief  Print a format.
  * @param  handle: the destination handle.
  * @param  format: the format to print.
  * @param  ...:  the list of arguments.
  * @retval Exit Status
  */
int canspy_print_format(void *handle, const char *format, ...){
	
	int len;
	va_list args;
	
	va_start(args, format);
	len = vsnprintf(NULL, 0, format, args);
	va_end(args);
	
	if(len > 0){
		
		char buf[len + 1];
		
		va_start(args, format);
		len = vsnprintf(buf, len + 1, format, args);
		va_end(args);
		
		return canspy_print_buffer(handle, buf, len);
	}
	
	return EXIT_FAILURE;
}

/**
  * @brief  Print a format line.
  * @param  handle: the destination handle.
  * @param  format: the format to print.
  * @param  ...:  the list of arguments.
  * @retval Exit Status
  */
int canspy_print_format_line(void *handle, const char *format, ...){
	
	int len;
	va_list args;
	
	va_start(args, format);
	len = vsnprintf(NULL, 0, format, args);
	va_end(args);
	
	if(len > 0){
		
		char buf[len + sizeof(CANSPY_PRINT_BREAK)];
		
		va_start(args, format);
		len = vsnprintf(buf, len + 1, format, args);
		va_end(args);
		
		strncpy(buf + len, CANSPY_PRINT_BREAK, sizeof(CANSPY_PRINT_BREAK));
		
		return canspy_print_buffer(handle, buf, len + sizeof(CANSPY_PRINT_BREAK) - 1);
	}
	
	return EXIT_FAILURE;
}

/**
  * @brief  Print a format in a specified buffer.
  * @param  buffer: the output buffer.
  * @param  size: the size of the output buffer.
  * @param  format: the format to print.
  * @param  ...:  the list of arguments.
  * @retval The length of the printed output
  */
int canspy_print_format_buffer(char *buffer, int size, const char *format, ...){
	
	int len;
	va_list args;
	
	va_start(args, format);
	len = vsnprintf(NULL, 0, format, args);
	va_end(args);
	
	if(len > 0 && (len + 1) <= size){
		
		va_start(args, format);
		vsnprintf(buffer, len + 1, format, args);
		va_end(args);
		
		return len;
	}
	
	return 0;
}

/**
  * @brief  Print a format in a newly allocated buffer.
  * @param  format: the format to print.
  * @param  ...:  the list of arguments.
  * @retval Exit Status
  */
char *canspy_print_format_alloc(const char *format, ...){
	
	int len;
	va_list args;
	char *out;
	
	va_start(args, format);
	len = vsnprintf(NULL, 0, format, args);
	va_end(args);
	
	if(len > 0){
		
		va_start(args, format);
		out = malloc(len + 1);
		
		if(out == NULL){
			
			CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
			return NULL;
		}
		
		vsnprintf(out, len + 1, format, args);
		va_end(args);
		
		return out;
	}
	
	return NULL;
}

/**
  * @brief  Print padding using tabular characters.
  * @param  handle: the destination handle.
  * @param  string: the string to pad.
  * @param  len: the length of the string (optional).
  * @param  p_max: a pointer to the expected maximum length.
  * @param  min: the minimum of tabulars to print.
  * @retval Exit Status
  */
int canspy_print_padding(void *handle, const char *string, int len, int *p_max, int min){
	
	int i;
	int nb;
	
	if(len == 0)
		len = strlen(string);
	
	if(len > *p_max)
		*p_max = len;
	
	nb = (*p_max / TAB_LEN) - (len / TAB_LEN) + min;
	
	for(i = 0; i < nb; i++)
		canspy_print_string(handle, (CANSPY_PRINT_PADCHAR), sizeof(CANSPY_PRINT_PADCHAR) - 1);
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Print a string and the corresponding padding.
  * @param  handle: the destination handle.
  * @param  string: the string to print and pad.
  * @param  len: the length of the string (optional).
  * @param  p_max: a pointer to the expected maximum length.
  * @param  min: the minimum of tabulars to print.
  * @retval Exit Status
  */
int canspy_print_padding_string(void *handle, const char *string, int len, int *p_max, int min){
	
	if(len == 0)
		len = strlen(string);
	
	return canspy_print_string(handle, string, len) | (*p_max != 0 ? canspy_print_padding(handle, string, len, p_max, min) : EXIT_SUCCESS);
}
