/**
  ******************************************************************************
  * @file    canspy_sdcard.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   SDCARD module core.
  *          This file provides all the functions needed by SDCARD services:
  *           + Message logging
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#include "canspy_sdcard.h"
#include "canspy_can.h"
#include "canspy_eth.h"
#include "canspy_pcap.h"
#include "canspy_uart.h"
#include "canspy_filter.h"
#include "canspy_print.h"
#include "canspy_debug.h"

/** @brief  Logical drive for SDCARD.
  */
char CANSPY_SDCARD_Drive[4];

/** @brief  Filesystem for SDCARD.
  */
FATFS CANSPY_SDCARD_Filesystem;

/** @brief  Absolute path SDCARD.
  */
char CANSPY_SDCARD_Path[8192];

/** @brief  Pointer to the current logging file.
  */
CANSPY_SDCARD_FIL *CANSPY_SDCARD_Log = NULL;

/** @brief  Shared variable to reduce the use of the stack.
  */
char CANSPY_SDCARD_NAME_Shared[_MAX_LFN + 1];

/**
  * @brief  The SD Card device handler.
  * @param  call: the call to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_sdcard_device(CANSPY_DEVICE_CALL call, CANSPY_OPTION_CALL *opt){
	
	if(call == DEV_INIT){
		
		MX_SDIO_SD_Init();
	}
	else if(call == DEV_START){
		
		if(FATFS_LinkDriver(&SD_Driver, CANSPY_SDCARD_Drive) !=0){
			
			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to link FATFS driver");
			return EXIT_FAILURE;
		}
				
		if(f_mount(&CANSPY_SDCARD_Filesystem, CANSPY_SDCARD_Drive, 1) != FR_OK){
			
			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to mount FATFS drive");
			return EXIT_FAILURE;
		}
	}
	else if(call == DEV_STOP){
		
		if(f_mount(NULL, CANSPY_SDCARD_Drive, 0) != FR_OK){
			
			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to unmount FATFS driver");
			return EXIT_FAILURE;
		}
		
		 if(FATFS_UnLinkDriver(CANSPY_SDCARD_Drive) != 0){
			
			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to unlink FATFS driver");
			return EXIT_FAILURE;
		}
	}
	else{
		
		CANSPY_DEBUG_ERROR_1(WARN, CANSPY_DEVICE_ERROR_Unknown, call);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

/**
  * @brief  Init a file structure.
  * @param  bfp: a pointer to the file structure to init.
	* @retval Exit Status
  */
void canspy_sdcard_init(CANSPY_SDCARD_FIL *bfp){
	
	memset(bfp, sizeof(*bfp), 0);
}

/**
  * @brief  Open a file structure.
  * @param  bfp: a pointer to the file structure to open.
  * @param  path: a pointer to the file name.
  * @param  mode: the access mode and file open mode flags.
  * @retval Exit Status
  */
int canspy_sdcard_open(CANSPY_SDCARD_FIL *bfp, const char* path, BYTE mode){
	
	if(f_open(&(bfp->fd), path, mode) == FR_OK)
		return EXIT_SUCCESS;
	else
		return EXIT_FAILURE;
}

/**
  * @brief  Read a buffer (no buffering).
  * @param  fp: a pointer to an open and valid internal file structure.
  * @param  buffer: the buffer to read.
  * @param  length: the length of the buffer.
  * @param  p_read: a pointer to store the read bytes.
  * @retval Exit Status
  */
int canspy_sdcard_direct_read(FIL *fp, void *buffer, int length, UINT *p_read){
	
	FRESULT result;
	
	do{
		
		result = f_read(fp, buffer, length, p_read);
	}
	while(result == FR_NOT_READY);
	
	if(result != FR_OK || *p_read == 0)
		return EXIT_FAILURE;
	else
		return EXIT_SUCCESS;
}

/**
  * @brief  Read a buffer.
  * @param  bfp: a pointer to an open and valid file structure.
  * @param  buffer: the buffer to read.
  * @param  length: the length of the buffer.
  * @param  p_read: a pointer to store the read bytes.
  * @retval Exit Status
  */
int canspy_sdcard_read(CANSPY_SDCARD_FIL *bfp, void *buffer, int length, UINT *p_read){

	int tmp_len;
	UINT tmp_read;
	
	if(bfp->buf_ptr == NULL){
		
		return canspy_sdcard_direct_read(&(bfp->fd), buffer, length, p_read);
	}
	else if(bfp->buf_ind == bfp->buf_len || bfp->buf_len == 0){

		if(canspy_sdcard_direct_read(&(bfp->fd), bfp->buf_ptr, bfp->buf_size, &(bfp->buf_len)) != EXIT_SUCCESS)
			return EXIT_FAILURE;
		else
			bfp->buf_ind = 0;
	}
	
	if(length > (tmp_len = bfp->buf_len - bfp->buf_ind)){
			
		memcpy(buffer, bfp->buf_ptr + bfp->buf_ind, tmp_len);
		bfp->buf_len = 0;
		*p_read = tmp_len;
		
		if(canspy_sdcard_read(bfp, (uint8_t *)buffer + tmp_len, length - tmp_len, &tmp_read) != EXIT_SUCCESS)
			return EXIT_FAILURE;
		else
			*p_read += tmp_read;
	}
	else{
		
		memcpy(buffer, bfp->buf_ptr + bfp->buf_ind, length);
		bfp->buf_ind += length;
		*p_read = length;
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Write a buffer (no buffering).
  * @param  fp: a pointer to an open and valid internal file structure.
  * @param  buffer: the buffer to write.
  * @param  length: the length of the buffer.
  * @param  flush: specifies whether to sync with the SDCARD or not.
  * @retval Exit Status
  */
int canspy_sdcard_direct_write(FIL *fp, void *buffer, int length, bool sync){
	
	FRESULT result;
	UINT written;
	
	while(length != 0 && buffer != NULL){
				
		result = f_write(fp, buffer, length, &written);
				
		if(result != FR_OK && result != FR_NOT_READY){

			CANSPY_DEBUG_ERROR_1(ERROR, "Failed to write on SDCARD: error %i", result);
			return EXIT_FAILURE;
		}
				
		if(result == FR_OK){
					
			length -= written;
			buffer = ((uint8_t *)buffer) + written;
		}
	}
		
	if(sync)
		f_sync(fp);
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Write a buffer.
  * @param  bfp: a pointer to an open and valid file structure.
  * @param  buffer: the buffer to write.
  * @param  length: the length of the buffer.
  * @param  flush: specifies whether to flush the buffer or not.
  * @retval Exit Status
  */
int canspy_sdcard_write(CANSPY_SDCARD_FIL *bfp, void *buffer, int length, bool flush){
	
	int tmp_len;
	
	if(bfp->buf_ptr == NULL){
		
		return canspy_sdcard_direct_write(&(bfp->fd), buffer, length, true);
	}
	else if(bfp->buf_ind == bfp->buf_size || (flush == true && bfp->buf_ind != 0)){
		
		if(canspy_sdcard_direct_write(&(bfp->fd), bfp->buf_ptr, bfp->buf_ind, true) != EXIT_SUCCESS)
			return EXIT_FAILURE;
		else
			bfp->buf_ind = 0;
	}
	
	if(length != 0 && buffer != NULL){
		
		if(length > (tmp_len = bfp->buf_size - bfp->buf_ind)){
		
			memcpy(bfp->buf_ptr + bfp->buf_ind, buffer, tmp_len);
			bfp->buf_ind += tmp_len;
			
			if(canspy_sdcard_write(bfp, (uint8_t *)buffer + tmp_len, length - tmp_len, false) != EXIT_SUCCESS)
				return EXIT_FAILURE;
		}
		else{
			
			memcpy(bfp->buf_ptr + bfp->buf_ind, buffer, length);
			bfp->buf_ind += length;
		}
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Close a file structure.
  * @param  bfp: a pointer to an open and valid file structure.
	* @retval Exit Status
  */
int canspy_sdcard_close(CANSPY_SDCARD_FIL *bfp){
	
	if(f_close(&(bfp->fd)) == FR_OK)
		return EXIT_SUCCESS;
	else
		return EXIT_FAILURE;
}

/**
  * @brief  The capture service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_sdcard_capture(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){

	SOCKETCAN can_frame;
	CANSPY_PCAP_HEADER_RECORD pcap_frame_header;
	
	static int fil_ctr = 1;
	static uint8_t write_buffer[UART_LINE_TX_BUF_SIZE];
	static CANSPY_SDCARD_FIL cap_fd;
	
	static char cap_prefix[_MAX_LFN + 1] = "can-";
	static bool cap_inj = false;
	static CANSPY_FILTER_ACT cap_fil = ACT_ANY;
	static bool cap_buf = false;

	if(flag == SVC_INIT){
		
		CANSPY_SERVICE_OPTION("cap_pre", CANSPY_OPTION_TYPES_Range[OPTION_STRING], CANSPY_OPTION_TYPES_Length[OPTION_STRING],
			cap_prefix, "The prefix used to create capture files", RESTART);
		CANSPY_SERVICE_OPTION("cap_inj", CANSPY_OPTION_TYPES_Range[OPTION_BOOL], CANSPY_OPTION_TYPES_Length[OPTION_BOOL],
			&cap_inj, "Specifies whether to capture injected CAN frames", INSTANT);
		CANSPY_SERVICE_OPTION("cap_fil", CANSPY_OPTION_TYPES_Range[OPTION_ACTION], CANSPY_OPTION_TYPES_Length[OPTION_ACTION],
			&cap_fil, "Specifies when to capture received CAN frames", INSTANT);
		CANSPY_SERVICE_OPTION("cap_buf", CANSPY_OPTION_TYPES_Range[OPTION_BOOL], CANSPY_OPTION_TYPES_Length[OPTION_BOOL],
			&cap_buf, "Specifies whether to rely on buffered capture operations", INSTANT);
	}
	else if(flag == SVC_START){
		
		canspy_sdcard_init(&cap_fd);
		
		do{
			
			canspy_print_format_buffer(CANSPY_SDCARD_NAME_Shared, sizeof(CANSPY_SDCARD_NAME_Shared), "%s%i.cap", cap_prefix, fil_ctr);
			fil_ctr++;
		}
		while(canspy_sdcard_open(&cap_fd, CANSPY_SDCARD_NAME_Shared, FA_CREATE_NEW | FA_WRITE) != EXIT_SUCCESS && fil_ctr < CANSPY_SDCARD_MAX_FILES);
		
		if(fil_ctr == CANSPY_SDCARD_MAX_FILES){
			
			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to open a capture file on the SDCARD");
			return EXIT_FAILURE;
		}
		else
			canspy_print_buffer(&cap_fd, &CANSPY_PCAP_FILE_Header, sizeof(CANSPY_PCAP_FILE_Header));
		
		if(cap_buf){
			
			cap_fd.buf_ptr = write_buffer;
			cap_fd.buf_size = sizeof(write_buffer);
		}
		else{
			
			cap_fd.buf_ptr = NULL;
			cap_fd.buf_size = 0;
		}
	}
	else if(flag == SVC_STOP){
		
		canspy_sdcard_write(&cap_fd, NULL, 0, true);
		canspy_sdcard_close(&cap_fd);
	}
	else if(flag == SVC_GET){
		
		if(CANSPY_OPTION_CMP(cap_prefix))
			CANSPY_OPTION_GET(string, cap_prefix);
		else if(CANSPY_OPTION_CMP(&cap_inj))
			CANSPY_OPTION_GET(bool, &cap_inj);
		else if(CANSPY_OPTION_CMP(&cap_fil))
			CANSPY_OPTION_GET(action, &cap_fil);
		else if(CANSPY_OPTION_CMP(&cap_buf))
			CANSPY_OPTION_GET(bool, &cap_buf);
	}
	else if(flag == SVC_SET){
		
		if(CANSPY_OPTION_CMP(cap_prefix))
			CANSPY_OPTION_SET(string, cap_prefix);
		else if(CANSPY_OPTION_CMP(&cap_inj))
			CANSPY_OPTION_SET(bool, &cap_inj);
		else if(CANSPY_OPTION_CMP(&cap_fil))
			CANSPY_OPTION_SET(action, &cap_fil);
		else if(CANSPY_OPTION_CMP(&cap_buf))
			CANSPY_OPTION_SET(bool, &cap_buf);
	}
	else if((flag == CAN1_RX || flag == CAN2_RX)
		&& (cap_fil == ACT_ANY || cap_fil == CANSPY_CAN_Match[flag - 1]
		|| (cap_fil == ACT_FWRD && CANSPY_CAN_Match[flag - 1] == ACT_ALTR))){
		
		canspy_can_wrap(&can_frame, canspy_sched_buffer(flag), true);
		canspy_pcap_write(&cap_fd, &can_frame, &pcap_frame_header, flag);
	}
	else if(cap_inj && flag == INJ_ETH){
		
		canspy_pcap_write(&cap_fd, &CANSPY_INJ_ETH_Sock, &pcap_frame_header, CANSPY_INJ_ETH_If);
	}
	else if(cap_inj && flag == INJ_SVC){
		
		canspy_pcap_write(&cap_fd, &CANSPY_INJ_SVC_Sock, &pcap_frame_header, CANSPY_INJ_SVC_If);
	}
	else if(cap_inj && flag == INJ_SVC){

		canspy_pcap_write(&cap_fd, &CANSPY_INJ_UART_Sock, &pcap_frame_header, CANSPY_INJ_UART_If);
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  The logging service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_sdcard_logging(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){

	static int fil_ctr = 1;
	static uint8_t write_buffer[UART_LINE_TX_BUF_SIZE];
	static CANSPY_SDCARD_FIL log_fd;
	
	static char log_prefix[_MAX_LFN + 1] = "evt-";
	static bool log_buf = false;
		
	if(flag == SVC_INIT){
		
		CANSPY_SERVICE_OPTION("log_pre", CANSPY_OPTION_TYPES_Range[OPTION_STRING], CANSPY_OPTION_TYPES_Length[OPTION_STRING],
			log_prefix, "The prefix used to create logging files", RESTART);
		CANSPY_SERVICE_OPTION("log_dbg", CANSPY_OPTION_TYPES_Range[OPTION_DEBUG], CANSPY_OPTION_TYPES_Length[OPTION_DEBUG],
			&CANSPY_DEBUG_SDCARD_Print, "Specifies the level of debug messages that should be printed in the logging files", INSTANT);
		CANSPY_SERVICE_OPTION("log_buf", CANSPY_OPTION_TYPES_Range[OPTION_BOOL], CANSPY_OPTION_TYPES_Length[OPTION_BOOL],
			&log_buf, "Specifies whether to rely on buffered logging operations", INSTANT);
	}
	else if(flag == SVC_START){
		
		canspy_sdcard_init(&log_fd);
		
		do{
			
			canspy_print_format_buffer(CANSPY_SDCARD_NAME_Shared, sizeof(CANSPY_SDCARD_NAME_Shared), "%s%i.log", log_prefix, fil_ctr);
			fil_ctr++;
		}
		while(canspy_sdcard_open(&log_fd, CANSPY_SDCARD_NAME_Shared, FA_CREATE_NEW | FA_WRITE) != EXIT_SUCCESS && fil_ctr < CANSPY_SDCARD_MAX_FILES);
		
		if(fil_ctr == CANSPY_SDCARD_MAX_FILES){
			
			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to open a logging file on SDCARD");
			return EXIT_FAILURE;
		}
		else{
			
			canspy_debug_stats(CANSPY_HANDLE_UART, CAN_IF1, true);
			canspy_debug_stats(CANSPY_HANDLE_UART, CAN_IF2, true);
			CANSPY_SDCARD_Log = &log_fd;
		}
		
		if(log_buf){
			
			log_fd.buf_ptr = write_buffer;
			log_fd.buf_size = sizeof(write_buffer);
		}
		else{
			
			log_fd.buf_ptr = NULL;
			log_fd.buf_size = 0;
		}
	}
	else if(flag == SVC_STOP){
	
		canspy_debug_stats(CANSPY_SDCARD_Log, CAN_IF1, false);
		canspy_debug_stats(CANSPY_SDCARD_Log, CAN_IF2, false);
		canspy_sdcard_write(&log_fd, NULL, 0, true);
		
		CANSPY_SDCARD_Log = NULL;
		canspy_sdcard_close(&log_fd);
	}
	else if(flag == SVC_GET){
		
		if(CANSPY_OPTION_CMP(log_prefix))
			CANSPY_OPTION_GET(string, log_prefix);
		else if(CANSPY_OPTION_CMP(&CANSPY_DEBUG_SDCARD_Print))
			CANSPY_OPTION_GET(debug, &CANSPY_DEBUG_SDCARD_Print);
		else if(CANSPY_OPTION_CMP(&log_buf))
			CANSPY_OPTION_GET(bool, &log_buf);
	}
	else if(flag == SVC_SET){
		
		if(CANSPY_OPTION_CMP(log_prefix))
			CANSPY_OPTION_SET(string, log_prefix);
		else if(CANSPY_OPTION_CMP(&CANSPY_DEBUG_SDCARD_Print))
			CANSPY_OPTION_SET(debug, &CANSPY_DEBUG_SDCARD_Print);
		else if(CANSPY_OPTION_CMP(&log_buf))
			CANSPY_OPTION_SET(bool, &log_buf);
	}
	else{
		
		/* Process flag using the debug event function */
		canspy_debug_event(flag, &log_fd);
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  The replay service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_sdcard_replay(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){

	uint8_t can_if;
	
	static uint8_t read_buffer[UART_LINE_TX_BUF_SIZE / 4];
	static CANSPY_SDCARD_FIL rep_fd;
	static CANSPY_PCAP_HEADER_RECORD cur_header;

	static CANSPY_FILTER_CAN fil_can = CAN_ANY;
	static char CANSPY_SDCARD_REPLAY_File[_MAX_LFN + 1] = "can-1.cap";
	static bool rep_inf = true;
	static bool rep_buf = false;
		
	if(flag == SVC_INIT){
		
		CANSPY_SERVICE_OPTION("rep_fil", CANSPY_OPTION_TYPES_Range[OPTION_STRING], CANSPY_OPTION_TYPES_Length[OPTION_STRING],
			CANSPY_SDCARD_REPLAY_File, "The capture file to replay", RESTART);
		CANSPY_SERVICE_OPTION("rep_can", CANSPY_OPTION_TYPES_Range[OPTION_CANIF], CANSPY_OPTION_TYPES_Length[OPTION_CANIF],
			&fil_can, "The CAN interface on which to replay frames", INSTANT);
		CANSPY_SERVICE_OPTION("rep_inf", CANSPY_OPTION_TYPES_Range[OPTION_BOOL], CANSPY_OPTION_TYPES_Length[OPTION_BOOL],
			&rep_inf, "Specifies whether to replay indefinitely (repeat loop)", INSTANT);
		CANSPY_SERVICE_OPTION("rep_buf", CANSPY_OPTION_TYPES_Range[OPTION_BOOL], CANSPY_OPTION_TYPES_Length[OPTION_BOOL],
			&rep_buf, "Specifies whether to rely on buffered read operations", RESTART);
	}
	else if(flag == SVC_START){
		
		canspy_sdcard_init(&rep_fd);
		
		if(canspy_sdcard_open(&rep_fd, CANSPY_SDCARD_REPLAY_File, FA_OPEN_EXISTING | FA_READ) != EXIT_SUCCESS || canspy_pcap_check(&rep_fd) != EXIT_SUCCESS)
			return EXIT_FAILURE;

		if(true){

			rep_fd.buf_ptr = read_buffer;
			rep_fd.buf_size = sizeof(read_buffer);
		}
		else{
			
			rep_fd.buf_ptr = NULL;
			rep_fd.buf_size = NULL;
		}
	}
	else if(flag == SVC_STOP){

		canspy_sdcard_close(&rep_fd);
	}
	else if(flag == SVC_GET){
		
		if(CANSPY_OPTION_CMP(CANSPY_SDCARD_REPLAY_File))
			CANSPY_OPTION_GET(string, CANSPY_SDCARD_REPLAY_File);
		else if(CANSPY_OPTION_CMP(&fil_can))
			CANSPY_OPTION_GET(canif, &fil_can);
		else if(CANSPY_OPTION_CMP(&rep_inf))
			CANSPY_OPTION_GET(bool, &rep_inf);
		else if(CANSPY_OPTION_CMP(&rep_buf))
			CANSPY_OPTION_GET(bool, &rep_buf);
	}
	else if(flag == SVC_SET){
		
		if(CANSPY_OPTION_CMP(CANSPY_SDCARD_REPLAY_File))
			CANSPY_OPTION_SET(string, CANSPY_SDCARD_REPLAY_File);
		else if(CANSPY_OPTION_CMP(&fil_can))
			CANSPY_OPTION_SET(canif, &fil_can);
		else if(CANSPY_OPTION_CMP(&rep_inf))
			CANSPY_OPTION_SET(bool, &rep_inf);
		else if(CANSPY_OPTION_CMP(&rep_buf))
			CANSPY_OPTION_SET(bool, &rep_buf);
	}
	else if(CANSPY_SCHED_Flags[INJ_SVC] == 0){
		
		if(canspy_pcap_walk(&rep_fd, &CANSPY_INJ_SVC_Sock, &cur_header, &can_if) == EXIT_SUCCESS){
			
			if(fil_can == CAN_ANY){
				
				if(can_if != 0)
					CANSPY_INJ_SVC_If = can_if;
				else
					return EXIT_FAILURE;
			}
			else
				CANSPY_INJ_SVC_If = fil_can;
			
			CANSPY_SCHED_SIGNAL(INJ_SVC);
		}
		else{
			
			if(rep_inf){
				
				CANSPY_SERVICE_RESTART();
			}
			else{
				
				CANSPY_SERVICE_NOMORE();
			}
		}
	}
	
	return EXIT_SUCCESS;
}
