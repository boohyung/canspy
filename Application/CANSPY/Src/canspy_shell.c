/**
  ******************************************************************************
  * @file    canspy_shell.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   Shell module extension.
  *          This file provides all the functions needed by the SHELL service:
  *           + Registering commands
  *           + Parsing command lines
	*           + Executing commands
	*           + Shell history
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#include "canspy_shell.h"
#include "canspy_uart.h"
#include "canspy_print.h"
#include "canspy_debug.h"

/** @brief  Buffer for SHELL history.
  */
char CANSPY_SHELL_HISTORY_Buf[CANSPY_SHELL_HISTORY][UART_LINE_RX_BUF_SIZE];

/** @brief  Iterator for SHELL history.
  */
int CANSPY_SHELL_HISTORY_Ptr = 0;

/** @brief  Storage for all registered shell commands
  */
CANSPY_SHELL_LIST CANSPY_SHELL_Commands = {0, NULL};

/**
  * @brief  Register a shell command.
  * @param  address: the calling address of the command.
  * @param  name: the name of the command.
  * @param  desc: the description of the command.
  * @param  help: the help message of the command.
  * @param  dev_id: a device ID to check for status (optional).
  * @retval Exit Status
  */
int canspy_shell_register(CANSPY_SHELL_PROC address, const char *name, const char *desc, const char *help, CANSPY_DEVICE_ID dev_id){

	int len;
	CANSPY_SHELL_CMD *cmd_list;

	cmd_list = realloc(CANSPY_SHELL_Commands.list, (CANSPY_SHELL_Commands.size + 1) * sizeof(CANSPY_SHELL_CMD));
	
	if(!cmd_list){
		
		CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
		return EXIT_FAILURE;
	}

	cmd_list[CANSPY_SHELL_Commands.size].name = name;
	cmd_list[CANSPY_SHELL_Commands.size].desc = desc;
	cmd_list[CANSPY_SHELL_Commands.size].help = help;
	cmd_list[CANSPY_SHELL_Commands.size].address = address;
	cmd_list[CANSPY_SHELL_Commands.size].dev_id = dev_id;
	CANSPY_SHELL_Commands.list = cmd_list;
	CANSPY_SHELL_Commands.size++;
	
	if((len = strlen(name)) > CANSPY_PRINT_MAX_Command)
		CANSPY_PRINT_MAX_Command = len;
	
	CANSPY_DEBUG_COMMAND(name, address);

	return EXIT_SUCCESS;
}

/**
  * @brief  Parse a shell command.
  * @param  command: the command to parse.
  * @param  p_argc: a pointer to the resulting argc
  * @param  p_argv: a pointer to the resulting argv (optional for memory assessment)
  * @retval The memory size needed for argv
  */
int canspy_shell_parse(char *command, int *p_argc, char **p_argv){

	char *p_args;
	char *current_pointer;
	char current_char;
	char inside_quote;
	char copy_char;
	int (byte_counter);
	int number_of_slashes;
	int memory_size;

	if(command == NULL || p_argc == NULL)
		return 0;

	if(p_argv != NULL){

		p_args = (char *)p_argv + (((*p_argc) + 1) * sizeof(char *));
		*p_argv = p_args;
		p_argv++;
	}
	else
		p_args = NULL;

	(byte_counter) = 0;
	*p_argc = 1;
	current_pointer = command;

	if(*current_pointer == '\"'){

		while((*(++current_pointer) != '\"') && (*current_pointer != '\0')){

			if(p_args != NULL){

				*p_args = *current_pointer;
				p_args++;
			}

			byte_counter++;
		}

		if(p_args != NULL){

			*p_args = '\0';
			p_args++;
		}

		if(*current_pointer == '\"')
			current_pointer++;

		byte_counter++;
	}
	else{

		do{

			if(p_args != NULL){

				*p_args = *current_pointer;
				p_args++;
			}

			current_char = (char)*current_pointer++;
			byte_counter++;
		}
		while(current_char != ' ' && current_char != '\0' && current_char != '\t');

		if(current_char != '\0'){
		
			if(p_args != NULL)
				*(p_args - 1) = '\0';
		}
		else
			current_pointer--;
	}

	inside_quote = 0;

	while(1){

		if(*current_pointer != '\0'){

			while(*current_pointer == ' ' || *current_pointer == '\t')
				current_pointer++;
		}
		else
			break;

		if(p_argv != NULL){

			*p_argv = p_args;
			p_argv++;
		}

		(*p_argc)++;

		while(1){
		
			copy_char = 1;
			number_of_slashes = 0;
		
			while(*current_pointer == '\\'){
		
				current_pointer++;
				number_of_slashes++;
			}
	
			if(*current_pointer == '\"'){

				if(number_of_slashes % 2 == 0){

					if(inside_quote == 1){

						if(current_pointer[1] == '\"')
							current_pointer++;
						else
							copy_char = 0;

						inside_quote = 0;
					}
					else{

						copy_char = 0;
						inside_quote = 1;
					}
				}
		
				number_of_slashes /= 2;
			}

			while(number_of_slashes--){

				if(p_args != NULL){

					*p_args = '\\';
					p_args++;
				}

				byte_counter++;
			}

			if(*current_pointer == '\0' || ((inside_quote == 0) && (*current_pointer == ' ' || *current_pointer == '\t')))
				break;

			if(copy_char == 1){

				if(p_args != NULL){

					*p_args = *current_pointer;
					p_args++;
				}

				byte_counter++;
			}
		
			current_pointer++;
		}

		if(p_args != NULL){

			*p_args = '\0';
			p_args++;
		}

		byte_counter++;
	}

	byte_counter--;

	if(p_argv != NULL)
		*p_argv++ = NULL;

	memory_size = (((*p_argc) + 1) * sizeof(char *)) + ((byte_counter + 1) * sizeof(char));
	
	return memory_size;
}

/**
  * @brief  Execute a shell command.
  * @param  command: the command to execute.
  * @retval Exit Status
  */
int canspy_shell_execute(char *command){

	int retval;
	int n_size;
	int n_argc;

	if(command == NULL)
		return EXIT_FAILURE;

	n_size = canspy_shell_parse(command, &n_argc, NULL);

	if(n_size <= 0){
		
		CANSPY_DEBUG_ERROR_0(ERROR, "Invalid command line");
		return EXIT_FAILURE;
	}
	else{
		
		char b_argv[n_size];
		char **n_argv;

		n_argv = (char **)b_argv;
		retval = EXIT_FAILURE;

		if(canspy_shell_parse(command, &n_argc, n_argv) == n_size){

			if(n_argc > 0){

				int i;

				for(i = 0; i < CANSPY_SHELL_Commands.size; i++){

					if(strncmp(CANSPY_SHELL_Commands.list[i].name, n_argv[0], CANSPY_PRINT_MAX_Command) == 0){

						if(CANSPY_SHELL_Commands.list[i].dev_id != CANSPY_NODEVICE && !CANSPY_DEVICE_POINTER(CANSPY_SHELL_Commands.list[i].dev_id)->powered){
							
							canspy_print_format_line(CANSPY_HANDLE_UART, "%s device not started", CANSPY_DEVICE_POINTER(CANSPY_SHELL_Commands.list[i].dev_id)->dev_name);
							return EXIT_FAILURE;
						}
												
						CANSPY_SHELL_Commands.list[i].address(n_argc, n_argv);
						retval = EXIT_SUCCESS;
						break;
					}
				}
				
				if(retval != EXIT_SUCCESS)
					canspy_print_format_line(CANSPY_HANDLE_UART, "Unknown command: %s", n_argv[0]);
			}
		}
		return retval;
	}
}

/**
  * @brief  Print the command prompt.
  * @retval Exit Status
  */
int canspy_shell_prompt(void){
	
	return CANSPY_UART_STATIC_STRING(CANSPY_SHELL_PROMPT);
}
