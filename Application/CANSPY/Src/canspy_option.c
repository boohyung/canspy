/**
  ******************************************************************************
  * @file    canspy_option.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   Option module core.
  *          This file provides all the functions to manage options:
  *           + Registering
	*           + Removing
  *           + Walking
	*           + Accessing
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#include "canspy_option.h"
#include "canspy_device.h"
#include "canspy_service.h"
#include "canspy_filter.h"
#include "canspy_can.h"
#include "canspy_eth.h"
#include "canspy_debug.h"

/** @brief  The default error message for unknown options.
  */
char *CANSPY_OPTION_ERROR_Unknown = "Unknown option at %08x";

/** @brief  The strings associated with option ranges.
  */
char *CANSPY_OPTION_TYPES_Range[OPTION_TYPES] = {
	
	"[TRUE|FALSE]",
	"[STRING]",
	"[INFO|WARN|ERROR|FATAL]",
	"[xx:xx:xx:xx:xx:xx]",
	"[UINT16]",
	"[125|250|500|1000]",
	"[AUTO|CAN1|CAN2|ALL]",
	"[ANY|DROP|FWRD]",
	"[NONE|ECHO|UART]"
};

/** @brief  The length associated with option strings.
  */
int CANSPY_OPTION_TYPES_Length[OPTION_TYPES] = {
	
	0,
	0,
	0,
	(sizeof(CANSPY_ETH_Mac[0]) * 3) + 2,
	(sizeof(CANSPY_ETH_Type[0]) * 3) + 2,
	(sizeof(uint16_t) * 3) + 2,
	0,
	0,
	0
};

/** @brief  The strings for boolean values.
  */
char *CANSPY_OPTION_BOOL_String[] = {"FALSE", "TRUE"};

/**
  * @brief  Register a new option.
  * @param  opt_ptr: a pointer to the option structure.
  * @param  var_name: the name of the option (MUST BE UNIQUE).
  * @param  var_addr: the address of the option variable (MUST BE UNIQUE).
  * @param  var_desc: the description of the option.
  * @param  var_length: the length needed for var_string (optional) 
  * @param  restart: specifies if modification mandates a restart.
  * @retval Exit Status
  */
int canspy_option_register(CANSPY_OPTION_DESC *opt_ptr, const char *var_name, void *var_addr, const char* var_range, const char *var_desc, int var_length, bool restart){
	
	int len;
	CANSPY_OPTION_CMP *var_list = realloc(opt_ptr->var_list, (opt_ptr->var_size + 1) * sizeof(CANSPY_OPTION_CMP));
	
	if(!var_list){
		
		CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
		return EXIT_FAILURE;
	}
		
	var_list[opt_ptr->var_size].var_id = opt_ptr->var_size;
	var_list[opt_ptr->var_size].var_name = var_name;
	var_list[opt_ptr->var_size].var_addr = var_addr;
	var_list[opt_ptr->var_size].var_range = var_range;
	var_list[opt_ptr->var_size].var_desc = var_desc;
	
	if((var_list[opt_ptr->var_size].var_length = var_length) !=0)
		var_list[opt_ptr->var_size].var_string = malloc(var_length);
	else
		var_list[opt_ptr->var_size].var_string = NULL;
	
	var_list[opt_ptr->var_size].new_value = NULL;
	var_list[opt_ptr->var_size].restart = restart;
	
	if((len = strlen(var_name)) > CANSPY_PRINT_MAX_Option)
		CANSPY_PRINT_MAX_Option = len;
	
	if((len = strlen(var_range)) > CANSPY_PRINT_MAX_Range)
		CANSPY_PRINT_MAX_Range = len;
	
	opt_ptr->var_list = var_list;
	opt_ptr->var_size++;
	
	if(var_length == 0 && (len = strlen(var_addr)) > CANSPY_PRINT_MAX_Value)
		CANSPY_PRINT_MAX_Value = len;
	else if(var_length > CANSPY_PRINT_MAX_Value)
		CANSPY_PRINT_MAX_Value = var_length - 1;
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Walk through all variables of a specified option.
  * @param  opt_ptr: the specified option.
  * @param  cur_var: the current variable or NULL to start.
  * @param  opt_func: the function to call to update var_string (optional).
  * @retval The next variable
  */
CANSPY_OPTION_CMP *canspy_option_walk(CANSPY_OPTION_DESC *opt_ptr, CANSPY_OPTION_CMP *cur_var, void *opt_func){

	CANSPY_OPTION_CMP *next_var;
	CANSPY_OPTION_CALL opt_call;
	
	if(opt_ptr == NULL || opt_ptr->var_size <= 0)
		next_var = NULL;
	else if(cur_var == NULL)
		next_var = &(opt_ptr->var_list[0]);
	else if(cur_var->var_id >= opt_ptr->var_size - 1)
		next_var = NULL;
	else
		next_var = &(opt_ptr->var_list[cur_var->var_id + 1]);
	
	if(opt_func != NULL && next_var != NULL){
		
		opt_call.var_ptr = next_var;
		((CANSPY_OPTION_FUNC)opt_func)(OPT_GET, &opt_call);
	}
	
	return next_var;
}

/**
  * @brief  Read or write an option variable.
  * @param  write: read or write access.
  * @param  var_name: the name of the variable.
  * @param  new_val: the new value of the variable (optional).
  * @retval The corresponding variable structure
  */
CANSPY_OPTION_CMP *canspy_option_variable(bool write, char *var_name, char *new_val){
	
	CANSPY_OPTION_CMP *cur_var = NULL;
	CANSPY_DEVICE_DESC *cur_dev = NULL;
	CANSPY_SERVICE_DESC *cur_svc = NULL;
	CANSPY_OPTION_CALL opt_call;
	
	while((cur_dev = canspy_device_walk(cur_dev)) != NULL){
		
		while((cur_var = canspy_option_walk(cur_dev->dev_opt, cur_var, NULL)) != NULL){
			
			if(strcasecmp(cur_var->var_name, var_name) == 0){
					
				opt_call.var_ptr = cur_var;

				if(!write)
					cur_dev->dev_func(DEV_GET, &opt_call);
				else{
					
					cur_var->new_value = new_val;
					cur_dev->dev_func(DEV_SET, &opt_call);
					
					/* Force the update of 'var_string' */
					cur_dev->dev_func(DEV_GET, &opt_call);
				}
				
				return cur_var;
			}
		}
		
		while((cur_svc = canspy_service_walk(cur_dev, cur_svc)) != NULL){
			
			while((cur_var = canspy_option_walk(cur_svc->svc_opt, cur_var, NULL)) != NULL){
				
				if(strcasecmp(cur_var->var_name, var_name) == 0){
							
					opt_call.var_ptr = cur_var;

					if(!write)
						cur_svc->svc_func(SVC_GET, &opt_call);
					else{
						
						cur_var->new_value = new_val;
						cur_svc->svc_func(SVC_SET, &opt_call);
						
						/* Force the update of 'var_string' */
						cur_svc->svc_func(SVC_GET, &opt_call);
					}
					
					return cur_var;
				}
			}
		}
	}
	
	return NULL;
}

/**
  * @brief  Handle options using the bool type.
  * @param  p1: a pointer to the string
  * @param  p2: a pointer to the variable.
  * @param  set: specifies whether to set or to get the value.
  * @retval None
  */
void canspy_option_bool(void *p1, void *p2, bool set){
	
	if(set){
		
		bool *new_val = p1;
		char **new_str = p2;
		
		if(strcasecmp(CANSPY_OPTION_BOOL_String[true], *new_str) == 0)
			*new_val = true;
		else if(strcasecmp(CANSPY_OPTION_BOOL_String[false], *new_str) == 0)
			*new_val = false;
	}
	else{
		
		bool *cur_val = p1;
		char **cur_str = p2;
		
		if(*cur_val == true)
			*cur_str = CANSPY_OPTION_BOOL_String[true];
		else if(*cur_val == false)
			*cur_str = CANSPY_OPTION_BOOL_String[false];
	}
}

/**
  * @brief  Handle options using the string type.
  * @param  p1: a pointer to the string
  * @param  p2: a pointer to the variable.
  * @param  set: specifies whether to set or to get the value.
  * @retval None
  */
void canspy_option_string(void *p1, void *p2, bool set){
	
	if(set){
		
		char *new_val = p1;
		char **new_str = p2;
		
		strcpy(new_val, *new_str);
		
	}
	else{
		
		char *cur_val = p1;
		char **cur_str = p2;
		
		*cur_str = cur_val;
	}
}

/**
  * @brief  Handle options describing a debug level.
  * @param  p1: a pointer to the string
  * @param  p2: a pointer to the variable.
  * @param  set: specifies whether to set or to get the value.
  * @retval None
  */
void canspy_option_debug(void *p1, void *p2, bool set){
	
	if(set){
		
		CANSPY_DEBUG_LEVEL *new_val = p1;
		char **new_str = p2;
		
		if(strcasecmp(CANSPY_DEBUG_LEVEL_String[DBG_INFO], *new_str) == 0)
			*new_val = DBG_INFO;
		else if(strcasecmp(CANSPY_DEBUG_LEVEL_String[DBG_WARN], *new_str) == 0)
			*new_val = DBG_WARN;
		else if(strcasecmp(CANSPY_DEBUG_LEVEL_String[DBG_ERROR], *new_str) == 0)
			*new_val = DBG_ERROR;
		else if(strcasecmp(CANSPY_DEBUG_LEVEL_String[DBG_FATAL], *new_str) == 0)
			*new_val = DBG_FATAL;
	}
	else{
		
		CANSPY_DEBUG_LEVEL *cur_val = p1;
		char **cur_str = p2;
		
		if(*cur_val == DBG_INFO)
			*cur_str = CANSPY_DEBUG_LEVEL_String[DBG_INFO];
		else if(*cur_val == DBG_WARN)
			*cur_str = CANSPY_DEBUG_LEVEL_String[DBG_WARN];
		else if(*cur_val == DBG_ERROR)
			*cur_str = CANSPY_DEBUG_LEVEL_String[DBG_ERROR];
		else if(*cur_val == DBG_FATAL)
			*cur_str = CANSPY_DEBUG_LEVEL_String[DBG_FATAL];
	}
}

/**
  * @brief  Handle options describing a MAC address.
  * @param  p1: a pointer to the string
  * @param  p2: a pointer to the variable.
  * @param  set: specifies whether to set or to get the value.
  * @retval None
  */
void canspy_option_macaddr(void *p1, void *p2, bool set){
	
	uint8_t tmp0, tmp1, tmp2, tmp3, tmp4, tmp5;
		
	if(set){
		
		uint8_t *new_val = p1;
		char **new_str = p2;
		
		if(sscanf(*new_str, "%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx", &tmp0, &tmp1, &tmp2, &tmp3, &tmp4, &tmp5) == 6){
				
			new_val[0] = tmp0;
			new_val[1] = tmp1;
			new_val[2] = tmp2;
			new_val[3] = tmp3;
			new_val[4] = tmp4;
			new_val[5] = tmp5;
		}
	}
	else{
		
		uint8_t *cur_val = p1;
		char **cur_str = p2;
		
		sprintf(*cur_str, "%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx", cur_val[0], cur_val[1], cur_val[2], cur_val[3], cur_val[4], cur_val[5]);
	}
}

/**
  * @brief  Handle options describing an Ether type.
  * @param  p1: a pointer to the string
  * @param  p2: a pointer to the variable.
  * @param  set: specifies whether to set or to get the value.
  * @retval None
  */
void canspy_option_ethtype(void *p1, void *p2, bool set){
	
	int tmp0;
		
	if(set){
		
		uint16_t *new_val = p1;
		char **new_str = p2;
		
		if((tmp0 = strtol(*new_str, NULL, 0)) != 0)
			*new_val = tmp0;
	}
	else{
		
		uint16_t *cur_val = p1;
		char **cur_str = p2;

		sprintf(*cur_str, "0x%04x", *cur_val);
	}
}

/**
  * @brief  Handle options describing a Ethernet acknowledgement.
  * @param  p1: a pointer to the string
  * @param  p2: a pointer to the variable.
  * @param  set: specifies whether to set or to get the value.
  * @retval None
  */
void canspy_option_ethack(void *p1, void *p2, bool set){
	
	if(set){
		
		CANSPY_ETH_ACK *new_val = p1;
		char **new_str = p2;
		
		if(strcasecmp(CANSPY_ETH_ACK_String[ACK_NONE], *new_str) == 0)
			*new_val = ACK_NONE;
		else if(strcasecmp(CANSPY_ETH_ACK_String[ACK_ECHO], *new_str) == 0)
			*new_val = ACK_ECHO;
		else if(strcasecmp(CANSPY_ETH_ACK_String[ACK_CMD], *new_str) == 0)
			*new_val = ACK_CMD;
	}
	else{
		
		CANSPY_ETH_ACK *cur_val = p1;
		char **cur_str = p2;
		
		if(*cur_val == ACK_NONE)
			*cur_str = CANSPY_ETH_ACK_String[ACK_NONE];
		else if(*cur_val == ACK_ECHO)
			*cur_str = CANSPY_ETH_ACK_String[ACK_ECHO];
		else if(*cur_val == ACK_CMD)
			*cur_str = CANSPY_ETH_ACK_String[ACK_CMD];
	}
}

/**
  * @brief  Handle options describing a CAN prescaler.
  * @param  p1: a pointer to the string
  * @param  p2: a pointer to the variable.
  * @param  set: specifies whether to set or to get the value.
  * @retval None
  */
void canspy_option_prescale(void *p1, void *p2, bool set){
	
	int tmp0;
		
	if(set){
		
		uint16_t *new_val = p1;
		char **new_str = p2;
		
		if((tmp0 = canspy_can_prescaler(strtol(*new_str, NULL, 0), false)) != 0)
			*new_val = tmp0;
	}
	else{
		
		uint16_t *cur_val = p1;
		char **cur_str = p2;

		sprintf(*cur_str, "%i", canspy_can_prescaler(*cur_val, true));
	}
}

/**
  * @brief  Handle options describing a CAN interface.
  * @param  p1: a pointer to the string
  * @param  p2: a pointer to the variable.
  * @param  set: specifies whether to set or to get the value.
  * @retval None
  */
void canspy_option_canif(void *p1, void *p2, bool set){
	
	/* In the case of an option, "AUTO" seems to be a better term than "ANY" */
	static char *can_auto = "AUTO";
		
	if(set){
		
		CANSPY_FILTER_CAN *new_val = p1;
		char **new_str = p2;
		
		if(strcasecmp(can_auto, *new_str) == 0)
			*new_val = CAN_ANY;
		else if(strcasecmp(CANSPY_FILTER_CAN_String[CAN_IF1], *new_str) == 0)
			*new_val = CAN_IF1;
		else if(strcasecmp(CANSPY_FILTER_CAN_String[CAN_IF2], *new_str) == 0)
			*new_val = CAN_IF2;
		else if(strcasecmp(CANSPY_FILTER_CAN_String[CAN_ALL], *new_str) == 0)
			*new_val = CAN_ALL;
	}
	else{
		
		CANSPY_FILTER_CAN *cur_val = p1;
		char **cur_str = p2;
		
		if(*cur_val == CAN_ANY)
			*cur_str = can_auto;
		else if(*cur_val == CAN_IF1)
			*cur_str = CANSPY_FILTER_CAN_String[CAN_IF1];
		else if(*cur_val == CAN_IF2)
			*cur_str = CANSPY_FILTER_CAN_String[CAN_IF2];
		else if(*cur_val == CAN_ALL)
			*cur_str = CANSPY_FILTER_CAN_String[CAN_ALL];
	}
}

/**
  * @brief  Handle options describing a filter action.
  * @param  p1: a pointer to the string
  * @param  p2: a pointer to the variable.
  * @param  set: specifies whether to set or to get the value.
  * @retval None
  */
void canspy_option_action(void *p1, void *p2, bool set){
	
	if(set){
		
		CANSPY_FILTER_ACT *new_val = p1;
		char **new_str = p2;
		
		if(strcasecmp(CANSPY_FILTER_ACT_String[ACT_DROP], *new_str) == 0)
			*new_val = ACT_DROP;
		else if(strcasecmp(CANSPY_FILTER_ACT_String[ACT_ALTR], *new_str) == 0)
			*new_val = ACT_ALTR;
		else if(strcasecmp(CANSPY_FILTER_ACT_String[ACT_FWRD], *new_str) == 0)
			*new_val = ACT_FWRD;
		else if(strcasecmp(CANSPY_FILTER_ACT_String[ACT_ANY], *new_str) == 0)
			*new_val = ACT_ANY;
	}
	else{
		
		CANSPY_FILTER_ACT *cur_val = p1;
		char **cur_str = p2;
		
		if(*cur_val == ACT_DROP)
			*cur_str = CANSPY_FILTER_ACT_String[ACT_DROP];
		else if(*cur_val == ACT_ALTR)
			*cur_str = CANSPY_FILTER_ACT_String[ACT_ALTR];
		else if(*cur_val == ACT_FWRD)
			*cur_str = CANSPY_FILTER_ACT_String[ACT_FWRD];
		else if(*cur_val == ACT_ANY)
			*cur_str = CANSPY_FILTER_ACT_String[ACT_ANY];
	}
}
