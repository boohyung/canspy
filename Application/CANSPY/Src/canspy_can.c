/**
  ******************************************************************************
  * @file    canspy_can.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   CAN module core.
  *          This file provides all the functions needed by CAN services:
  *           + Filtering
  *           + Forwarding
  *           + Injecting
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#include "canspy_can.h"
#include "canspy_eth.h"
#include "canspy_print.h"
#include "canspy_debug.h"

/** @brief  The default error messages for CAN buffer and variables failures.
  */
char *CANSPY_CAN_ERROR_Buffer = "Failed to get CAN buffer";
char *CANSPY_CAN_ERROR_Variable = "Failed to get CAN variables";

/** @brief  Arrays of MsgTypeDef for CAN1 and CAN2.
  */
CanRxMsgTypeDef *CANSPY_CAN_Rx_Mess[] = {NULL, &CAN1_Rx_Mess, &CAN2_Rx_Mess};
CanRxMsgTypeDef *CANSPY_CAN_RxTx_Mess[] = {NULL, &CAN2_Rx_Mess, &CAN1_Rx_Mess};
CanTxMsgTypeDef *CANSPY_CAN_Tx_Mess[] = {NULL, &CAN1_Tx_Mess, &CAN2_Tx_Mess};
CanTxMsgTypeDef *CANSPY_CAN_TxRx_Mess[] = {NULL, &CAN2_Tx_Mess, &CAN1_Tx_Mess};

/** @brief  Arrays of matched filter rule for CAN1 and CAN2.
  */
CANSPY_FILTER_ACT CANSPY_CAN_Match[] = {ACT_ANY, ACT_ANY};

/** @brief  Variables used for injection.
  */
CANSPY_ETH_ACK CANSPY_INJ_ETH_Ack = ACK_NONE;
uint8_t CANSPY_INJ_ETH_If;
uint8_t CANSPY_INJ_SVC_If;
uint8_t CANSPY_INJ_UART_If;
SOCKETCAN CANSPY_INJ_ETH_Sock;
SOCKETCAN CANSPY_INJ_SVC_Sock;
SOCKETCAN CANSPY_INJ_UART_Sock;
bool CANSPY_INJ_ETH_Async = true;
bool CANSPY_INJ_SVC_Async = true;
bool CANSPY_INJ_UART_Async = true;

/** @brief  CAN services for injection.
  */
CANSPY_SERVICE_DESC *CANSPY_INJ_Services[] = {NULL, NULL};

/** @brief  Common filtering options for CAN services.
  */
CANSPY_FILTER_DESC fil_can = {NULL, 0};
bool filtered_forwarding[] = {false, false};
bool silent_fwrd[] = {false, false};
bool dummy_drop[] = {false, false};
#ifdef CANSPY_DEBUG_FILTER
uint16_t match_rule_s_id[] = {0, 0};
uint16_t match_rule_d_id[] = {0, 0};
#endif

/**
  * @brief  The CAN device handler.
  * @param  can: the device device number.
  * @param  call: the call to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_can_device(uint8_t can, CANSPY_DEVICE_CALL call, CANSPY_OPTION_CALL *opt){
	
	HAL_StatusTypeDef (*pMX_CAN_Init)(void);
	CanRxMsgTypeDef *pCAN_Rx_Mess;
	CanTxMsgTypeDef *pCAN_Tx_Mess;
	CAN_HandleTypeDef *pCAN_Hdl;
	uint16_t *pCAN_Prescaler;
	
	if(canspy_can_variables(can, &pMX_CAN_Init, NULL, &pCAN_Rx_Mess, &pCAN_Tx_Mess, &pCAN_Hdl, &pCAN_Prescaler) != EXIT_SUCCESS){
		
		CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Variable);
		return EXIT_FAILURE;
	}
	
	if(call == DEV_INIT){
		
		CANSPY_CANDEV_OPTION(can, "can%i_spd", CANSPY_OPTION_TYPES_Range[OPTION_PRESCALE], CANSPY_OPTION_TYPES_Length[OPTION_PRESCALE],
			pCAN_Prescaler, "The speed of the CAN%i bus", RESTART);
		CANSPY_CANDEV_OPTION(can, "can%i_mac", CANSPY_OPTION_TYPES_Range[OPTION_MACADDR], CANSPY_OPTION_TYPES_Length[OPTION_MACADDR],
			CANSPY_ETH_Mac[can], "The MAC address of the CAN%i interface over Ethernet", INSTANT);
		CANSPY_CANDEV_OPTION(can, "can%i_typ", CANSPY_OPTION_TYPES_Range[OPTION_ETHTYPE], CANSPY_OPTION_TYPES_Length[OPTION_ETHTYPE],
			&(CANSPY_ETH_Type[can]), "The EtherType of the CAN%i interface over Ethernet", INSTANT);
		CANSPY_CANDEV_OPTION(can, "can%i_sil", CANSPY_OPTION_TYPES_Range[OPTION_BOOL], CANSPY_OPTION_TYPES_Length[OPTION_BOOL],
			&(silent_fwrd[can - 1]), "Specifies if the CAN%i interface should become silent until the frame is sent", INSTANT);
	}
	else if(call == DEV_START){
		
		if(pMX_CAN_Init() != HAL_OK){
			
			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to init CAN HAL");
			return EXIT_FAILURE;
		}
		
		pCAN_Hdl->pRxMsg = pCAN_Rx_Mess;
		pCAN_Hdl->pTxMsg = pCAN_Tx_Mess;
		pCAN_Rx_Mess->FIFONumber = CAN_FIFO0;

		while(HAL_CAN_Receive_IT(pCAN_Hdl, CAN_FIFO0) != HAL_OK);
	}
	else if(call == DEV_STOP){
		
		if(HAL_CAN_DeInit(pCAN_Hdl) != HAL_OK)
			return EXIT_FAILURE;
		
		/* Dirty hack to deal with CAN clocks dependency */
		__CAN1_CLK_ENABLE();
		__CAN2_CLK_ENABLE();
	}
	else if(call == DEV_GET){

		if(CANSPY_OPTION_CMP(pCAN_Prescaler))
			CANSPY_OPTION_GET(prescale, pCAN_Prescaler);
		else if(CANSPY_OPTION_CMP(&(CANSPY_ETH_Mac[can])))
			CANSPY_OPTION_GET(macaddr, CANSPY_ETH_Mac[can]);
		else if(CANSPY_OPTION_CMP(&(CANSPY_ETH_Type[can])))
			CANSPY_OPTION_GET(ethtype, &(CANSPY_ETH_Type[can]));
		else if(CANSPY_OPTION_CMP(&(silent_fwrd[can - 1])))
			CANSPY_OPTION_GET(bool, &(silent_fwrd[can - 1]));
		else{

			CANSPY_DEBUG_ERROR_1(WARN, CANSPY_OPTION_ERROR_Unknown, CANSPY_OPTION_VAR);
			return EXIT_FAILURE;
		}
	}
	else if(call == DEV_SET){
		
		if(CANSPY_OPTION_CMP(pCAN_Prescaler))
			CANSPY_OPTION_SET(prescale, pCAN_Prescaler);
		else if(CANSPY_OPTION_CMP(&(CANSPY_ETH_Mac[can])))
			CANSPY_OPTION_SET(macaddr, CANSPY_ETH_Mac[can]);
		else if(CANSPY_OPTION_CMP(&(CANSPY_ETH_Type[can])))
			CANSPY_OPTION_SET(ethtype, &(CANSPY_ETH_Type[can]));
		else if(CANSPY_OPTION_CMP(&(silent_fwrd[can - 1]))){
			
			CANSPY_OPTION_SET(bool, &(silent_fwrd[can - 1]));
			
			/* If this option is no longer true, make sure the corresponding device is not in a silence state */
			if(!silent_fwrd[can - 1]){
				
				if(canspy_can_variables(can, NULL, NULL, NULL, NULL, &pCAN_Hdl, NULL) != EXIT_SUCCESS){
		
					CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Variable);
					return EXIT_FAILURE;
				}

				canspy_can_silence(pCAN_Hdl, false);
			}
		}
		else{

			CANSPY_DEBUG_ERROR_1(WARN, CANSPY_OPTION_ERROR_Unknown, CANSPY_OPTION_VAR);
			return EXIT_FAILURE;
		}
	}
	else{

		CANSPY_DEBUG_ERROR_1(WARN, CANSPY_DEVICE_ERROR_Unknown, call);
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  CAN1 wrapper for the device handler.
  * @param  call: the call to process.
  * @param  opt: an optionnal parameter.
  * @retval None
  */
int canspy_can_device1(CANSPY_DEVICE_CALL call, CANSPY_OPTION_CALL *opt){
	
	return canspy_can_device(CAN1_IF, call, opt);
}

/**
  * @brief  CAN2 wrapper for the device handler.
  * @param  call: the call to process.
  * @param  opt: an optionnal parameter.
  * @retval None
  */
int canspy_can_device2(CANSPY_DEVICE_CALL call, CANSPY_OPTION_CALL *opt){
	
	return canspy_can_device(CAN2_IF, call, opt);
}

/**
  * @brief  Handle the silence of a CAN device.
  * @param  pCAN_Hdl: a pointer to the CAN device handler.
  * @param  silent: specifies whether to go silent or not.
  * @retval None
  */
void canspy_can_silence(CAN_HandleTypeDef *pCAN_Hdl, bool silent){
	
	if(!silent && pCAN_Hdl->Init.Mode == CAN_MODE_SILENT){

		pCAN_Hdl->Init.Mode = CAN_MODE_NORMAL;
		HAL_CAN_Init(pCAN_Hdl);
	}
	else if(silent && pCAN_Hdl->Init.Mode == CAN_MODE_NORMAL){

		pCAN_Hdl->Init.Mode = CAN_MODE_SILENT;
		HAL_CAN_Init(pCAN_Hdl);
	}
}

/**
  * @brief  Defined as CAN_Rx_Handler.
  * @param  can_if: the CAN interface.
  * @retval Handled code
  */
int canspy_can_handler_rx(uint8_t can_if){
	
	CANSPY_DEBUG_COUNT_Rx[can_if - 1]++;
	CANSPY_CAN_Match[can_if - 1] = ACT_ANY;
	
	if(can_if == CAN1_IF){
		
		if(silent_fwrd[CAN1_IF - 1])
			canspy_can_silence(&hcan1, true);
		
		CANSPY_SCHED_SIGNAL(CAN1_RX);
	}
	else if(can_if == CAN2_IF){

		if(silent_fwrd[CAN2_IF - 1])
			canspy_can_silence(&hcan2, true);
		
		CANSPY_SCHED_SIGNAL(CAN2_RX);
	}
	else
		CANSPY_DEBUG_ERROR_0(ERROR, "Wrong CAN interface");
	
	return 0;
}

/**
  * @brief  Defined as CAN_Tx_Handler.
  * @param  can_if: the CAN interface.
  * @retval Handled code
  */
int canspy_can_handler_tx(uint8_t can_if){
	
	CANSPY_DEBUG_COUNT_Tx[can_if - 1]++;
	
	return 0;
}

/**
  * @brief  Get the variables associated with a CAN device.
  * @param  can: the CAN device.
  * @param  ppMX_CAN_Init: a double-pointer to the initializer.
  * @param  pCAN_Rx_Flag: a pointer to the Rx flag index.
  * @param  ppCAN_Rx_Mess: a double-pointer to the Rx buffer.
  * @param  ppCAN_Tx_Mess: a double-pointer to the Tx buffer.
  * @param  ppCAN_Hdl: a double-pointer to the device handler.
  * @retval Exit Status
  */
int canspy_can_variables(uint8_t can, HAL_StatusTypeDef (**ppMX_CAN_Init)(void), CANSPY_SERVICE_FLAG *pCAN_Rx_Flag, CanRxMsgTypeDef **ppCAN_Rx_Mess, CanTxMsgTypeDef **ppCAN_Tx_Mess, CAN_HandleTypeDef **ppCAN_Hdl, uint16_t **ppCAN_Prescaler){
	
	if(can == CAN1_IF){

		if(ppMX_CAN_Init != NULL)
			*ppMX_CAN_Init = MX_CAN1_Init;

		if(pCAN_Rx_Flag != NULL)
			*pCAN_Rx_Flag = CAN1_RX;
		
		if(ppCAN_Rx_Mess != NULL)
			*ppCAN_Rx_Mess = &CAN1_Rx_Mess;
		
		if(ppCAN_Tx_Mess != NULL)
			*ppCAN_Tx_Mess = &CAN1_Tx_Mess;
		
		if(ppCAN_Hdl != NULL)
			*ppCAN_Hdl = &hcan1;
		
		if(ppCAN_Prescaler != NULL)
			*ppCAN_Prescaler = &CAN1_Prescaler;
	}
	else if(can == CAN2_IF){
		
		if(ppMX_CAN_Init != NULL)
			*ppMX_CAN_Init = MX_CAN2_Init;
		
		if(pCAN_Rx_Flag != NULL)
			*pCAN_Rx_Flag = CAN2_RX;
		
		if(ppCAN_Rx_Mess != NULL)
			*ppCAN_Rx_Mess = &CAN2_Rx_Mess;
		
		if(ppCAN_Tx_Mess != NULL)
			*ppCAN_Tx_Mess = &CAN2_Tx_Mess;
		
		if(ppCAN_Hdl != NULL)
			*ppCAN_Hdl = &hcan2;
		
		if(ppCAN_Prescaler != NULL)
			*ppCAN_Prescaler = &CAN2_Prescaler;
	}
	else{
		
		CANSPY_DEBUG_ERROR_1(ERROR, "%i: unknown CAN interface", can);
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Get the prescaler associated with a CAN bitrate.
  * @param  reverse: compute bitrate instead.
  * @retval Prescaler or bitrate
  */
uint16_t canspy_can_prescaler(uint16_t val, bool reverse){
	
	int s, d;
	uint16_t bitrate_list[][2] = {{2, 1000}, {4, 500}, {8, 250}, {16, 125}};
	
	if(reverse){
		
		s = 0;
		d = 1;
	}
	else{
		
		s = 1;
		d = 0;
	}
	
	for(int i = 0; i < sizeof(bitrate_list) / sizeof(*bitrate_list); i++){
		
		if(bitrate_list[i][s] == val)
			return bitrate_list[i][d];
	}
	
	return 0;
}

/**
  * @brief  Wrap a CAN message in a SocketCAN structure.
  * @param  pRxMess: the CAN message.
  * @param  pcan_frame: the SocketCAN structure.
  * @param  network: specifies network byte order conversion.
  * @retval None
  */
void canspy_can_wrap(SOCKETCAN *pcan_frame, CanRxMsgTypeDef *pRxMess, bool network){

	/* Zeroing the initial frame */
	memset(pcan_frame, 0, sizeof(*pcan_frame));
	
	/* Setting the header */
	if(pRxMess->IDE == CAN_ID_EXT){
		
		pcan_frame->id = pRxMess->ExtId;
		pcan_frame->id |= SOCKETCAN_EFF;
	}
	else
		pcan_frame->id = pRxMess->StdId;
	
	if(pRxMess->RTR == CAN_RTR_REMOTE)
		pcan_frame->id |= SOCKETCAN_RTR;
	
	/*if(false) //TODO: handle error frame
		pcan_frame->id |= SOCKETCAN_ERR;*/
	
	/* Convert from host to network byte order if specified */
	if(network)
		pcan_frame->id = htonl(pcan_frame->id);
	
	/* Copying data length code*/
	pcan_frame->dlc = pRxMess->DLC;
	
	/* copying data if not RTR */
	if(pRxMess->RTR == CAN_RTR_DATA)
		memcpy(pcan_frame->data, pRxMess->Data, pRxMess->DLC);
}

/**
  * @brief  Unwrap a CAN message from a SocketCAN structure.
  * @param  pRxMess: the CAN message.
  * @param  pcan_frame: the SocketCAN structure.
  * @param  network: specifies network byte order conversion.
  * @retval None
  */
void canspy_can_unwrap(CanRxMsgTypeDef *pRxMess, SOCKETCAN *pcan_frame, bool network){
	
	uint32_t tmp_id = pcan_frame->id;
	
	/* Convert from network to host byte order if specified */
	if(network)
		tmp_id = ntohl(tmp_id);
	
	/* Getting the header */
	if(tmp_id & SOCKETCAN_EFF){
		
		pRxMess->StdId = 0;
		pRxMess->ExtId = tmp_id & SOCKETCAN_MASK;
		pRxMess->IDE = CAN_ID_EXT;
		return;
	}
	else{
		
		pRxMess->ExtId = 0;
		pRxMess->StdId = tmp_id & SOCKETCAN_MASK;
		pRxMess->IDE = CAN_ID_STD;
	}
	
	if(tmp_id & SOCKETCAN_RTR)
		pRxMess->RTR = CAN_RTR_REMOTE;
	else
		pRxMess->RTR = CAN_RTR_DATA;
	
	/* Copying data length code*/
	pRxMess->DLC = pcan_frame->dlc;
	
	/* copying data if not RTR */
	if(pRxMess->RTR == CAN_RTR_DATA)
		memcpy(pRxMess->Data, pcan_frame->data, pcan_frame->dlc);
}

/**
  * @brief  Rewrap a SocketCAN structure with network byte order conversion.
  * @param  pcan_frame: a pointer to the SocketCAN structure to rewrap.
  * @retval The structure provided as input. 
  */
SOCKETCAN *canspy_can_rewrap(SOCKETCAN *pcan_frame){
	
	pcan_frame->id = ntohl(pcan_frame->id);
	
	return pcan_frame;
}

/**
  * @brief  Send a frame on a CAN device.
  * @param  async: specifies whether the operation should be synchrone.
  * @param  CanDeviceId: the CAN device ID.
  * @param  pCAN_Hdl: a pointer to the CAN device handler.
  * @param  pCAN_Tx_Mess: a pointer to the Tx buffer.
  * @param  pCAN_Rx_Mess: a pointer to the Rx buffer (optional).
  * @retval Exit Status
  */
int canspy_can_send(bool async, CANSPY_DEVICE_ID CanDeviceId, CAN_HandleTypeDef *pCAN_Hdl, CanTxMsgTypeDef *pCAN_Tx_Mess, CanRxMsgTypeDef *pCANS_Rx_Mess){
	
	bool silent_mode = false;
	HAL_StatusTypeDef res;
	
	if(!CANSPY_DEVICE_ACTIVE(CanDeviceId))
		return EXIT_FAILURE;
	
	if(pCANS_Rx_Mess != NULL){
	
		pCAN_Tx_Mess->StdId = pCANS_Rx_Mess->StdId;
		pCAN_Tx_Mess->ExtId = pCANS_Rx_Mess->ExtId;
		pCAN_Tx_Mess->IDE = pCANS_Rx_Mess->IDE;
		pCAN_Tx_Mess->RTR = pCANS_Rx_Mess->RTR;
		pCAN_Tx_Mess->DLC = pCANS_Rx_Mess->DLC;
		
		memcpy(pCAN_Tx_Mess->Data, pCANS_Rx_Mess->Data, pCANS_Rx_Mess->DLC);
	}
	
	if(pCAN_Hdl->Init.Mode == CAN_MODE_SILENT){

		canspy_can_silence(pCAN_Hdl, false);
		silent_mode = true;
	}
	
	pCAN_Hdl->pTxMsg = pCAN_Tx_Mess;
	
	if(async)
		while((res = HAL_CAN_Transmit_IT(pCAN_Hdl)) == HAL_BUSY);
	else
		while((res = HAL_CAN_Transmit(pCAN_Hdl, CAN_TX_TIMEOUT)) == HAL_BUSY);
	
	if(silent_mode)
		canspy_can_silence(pCAN_Hdl, true);
	
	if(res != HAL_OK){

		CANSPY_DEBUG_ERROR_1(ERROR, "Failed to transmit CAN frames: %i", res);
		CANSPY_DEBUG_COUNT_Fail[CanDeviceId - 1]++;

		return EXIT_FAILURE;
	}
	else
		return EXIT_SUCCESS;
}

/**
  * @brief  Send a dummy frame on a CAN device.
  * @param  sync: specifies whether the operation should be synchrone.
  * @param  CanDeviceId: the CAN device ID.
  * @param  pCAN_Hdl: a pointer to the CAN device handler.
  * @param  pCAN_Tx_Mess: a pointer to the Tx buffer.
  * @param  pCAN_Rx_Mess: a pointer to the Rx buffer.
  * @retval None
  */
void canspy_can_dummy(bool sync, CANSPY_DEVICE_ID CanDeviceId, CAN_HandleTypeDef *pCAN_Hdl, CanTxMsgTypeDef *pCAN_Tx_Mess, CanRxMsgTypeDef *pCANS_Rx_Mess){
	
	CanTxMsgTypeDef TMP_Tx_Mess;
	
	if(pCANS_Rx_Mess == NULL || pCANS_Rx_Mess->RTR == CAN_RTR_REMOTE)
		return;
	
	TMP_Tx_Mess.StdId = pCANS_Rx_Mess->StdId;
	TMP_Tx_Mess.ExtId = pCANS_Rx_Mess->ExtId;
	TMP_Tx_Mess.IDE = pCANS_Rx_Mess->IDE;
	TMP_Tx_Mess.RTR = pCANS_Rx_Mess->RTR;
	TMP_Tx_Mess.DLC = pCANS_Rx_Mess->DLC;
	
	memset(TMP_Tx_Mess.Data, 0, pCANS_Rx_Mess->DLC);
	
	canspy_can_send(sync, CanDeviceId, pCAN_Hdl, &TMP_Tx_Mess, NULL);
}

/**
  * @brief  The filter service.
  * @param  can_s: the CAN source
  * @param  can_d: the CAN destination
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_can_filter(uint8_t can_s, uint8_t can_d, CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){
	
	CANSPY_SERVICE_FLAG CAND_Rx_Flag;
	CanTxMsgTypeDef *pCAND_Tx_Mess;
	CAN_HandleTypeDef *pCAND_Hdl;
	CAN_HandleTypeDef *pCANS_Hdl;
	CanRxMsgTypeDef *can_rx;
	CANSPY_FILTER_RULE *cur_rul;
	CANSPY_FILTER_ACT *cur_act;

	static CANSPY_FILTER_ACT def_act[CAN2_IF] = {ACT_DROP, ACT_DROP};
	
	if(flag == SVC_INIT){
		
		CANSPY_CANSVC_OPTION(can_s, "flt%i_rul", "[FILTER COMMAND]", 0,
			&fil_can, "The list of rules used to filter CAN%i frames", INSTANT);
		CANSPY_CANSVC_OPTION(can_s, "flt%i_def", CANSPY_OPTION_TYPES_Range[OPTION_ACTION], CANSPY_OPTION_TYPES_Length[OPTION_ACTION],
			&(def_act[can_s - 1]), "The default action (rule 0) for CAN%i frames when no rules matches", INSTANT);
		CANSPY_CANSVC_OPTION(can_s, "flt%i_dmy", CANSPY_OPTION_TYPES_Range[OPTION_BOOL], CANSPY_OPTION_TYPES_Length[OPTION_BOOL],
			&(dummy_drop[can_s - 1]), "Specifies if a dummy frame should be sent when dropping a frame from CAN%i", INSTANT);
	}
	else if(flag == SVC_START){
		
		filtered_forwarding[can_d - 1] = true;
	}
	else if(flag == SVC_STOP){
		
		filtered_forwarding[can_d - 1] = false;
		
		if(canspy_can_variables(can_s, NULL, NULL, NULL, NULL, &pCANS_Hdl, NULL) != EXIT_SUCCESS){
			
			CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Variable);
			return EXIT_FAILURE;
		}

		canspy_can_silence(pCANS_Hdl, false);
	}
	else if(flag == SVC_GET){
		
		if(CANSPY_OPTION_CMP(&(def_act[can_s - 1])))
			CANSPY_OPTION_GET(action, &(def_act[can_s - 1]));
		else if(CANSPY_OPTION_CMP(&(dummy_drop[can_s - 1])))
			CANSPY_OPTION_GET(bool, &(dummy_drop[can_s - 1]));
		else if(CANSPY_OPTION_CMP(&fil_can))
			CANSPY_OPTION_STR = "N/A";
	}
	else if(flag == SVC_SET){
		
		if(CANSPY_OPTION_CMP(&(def_act[can_s - 1]))){
			
			CANSPY_OPTION_SET(action, &(def_act[can_s - 1]));
			
			/* Only DROP and FWRD are allowed as the default action */
			if(def_act[can_s - 1] != ACT_DROP)
				def_act[can_s - 1] = ACT_FWRD;
		}
		else if(CANSPY_OPTION_CMP(&(dummy_drop[can_s - 1])))
			CANSPY_OPTION_SET(bool, &(dummy_drop[can_s - 1]));
	}
	else if(flag == can_s){
	
		if(canspy_can_variables(can_d, NULL, &CAND_Rx_Flag, NULL, &pCAND_Tx_Mess, &pCAND_Hdl, NULL) != EXIT_SUCCESS){
			
			CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Variable);
			return EXIT_FAILURE;
		}
			
		if((can_rx = canspy_sched_buffer(flag)) == NULL){
			
			CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Buffer);
			return EXIT_FAILURE;
		}
		
		cur_rul = NULL;
		
		while((cur_rul = canspy_filter_walk(&fil_can, cur_rul)) != NULL)	
			if(canspy_filter_match(can_s, cur_rul, can_rx) == EXIT_SUCCESS)
				break;
			
		if(cur_rul != NULL){
			
			cur_act = &(cur_rul->can_act);
#ifdef CANSPY_DEBUG_FILTER
			match_rule_s_id[can_s - 1] = cur_rul->rul_id + 1;
			match_rule_d_id[can_d - 1] = cur_rul->rul_id + 1;
#endif
		}
		else{
			
			cur_act = &(def_act[can_d - 1]);
#ifdef CANSPY_DEBUG_FILTER
			match_rule_s_id[can_s - 1] = 0;
			match_rule_d_id[can_d - 1] = 0;
#endif
		}
		
		CANSPY_CAN_Match[can_s - 1] = *cur_act;

		switch(*cur_act){
			
			case ACT_DROP:
				CANSPY_DEBUG_COUNT_Drop[can_s - 1]++;
#ifdef CANSPY_DEBUG_FILTER
				CANSPY_SCHED_SIGNAL(DROP_CAN + can_s);
#endif
				if(dummy_drop[can_s - 1])
					canspy_can_dummy(ASYNC, (CANSPY_DEVICE_ID)CAND_Rx_Flag, pCAND_Hdl, pCAND_Tx_Mess, can_rx);
				break;
			case ACT_ALTR:
				if(canspy_filter_alter(cur_rul, pCAND_Tx_Mess, can_rx) == EXIT_SUCCESS){
					
					CANSPY_DEBUG_COUNT_Altr[can_s - 1]++;
					can_rx = NULL;
#ifdef CANSPY_DEBUG_FILTER
					CANSPY_SCHED_SIGNAL(ALTR_CAN + can_s);
#endif
				}
				else{

					CANSPY_DEBUG_ERROR_0(ERROR, "Failed to alter the CAN frame");
					return EXIT_FAILURE;
				}
			case ACT_FWRD:
			case ACT_ANY:
				canspy_can_send(ASYNC, (CANSPY_DEVICE_ID)CAND_Rx_Flag, pCAND_Hdl, pCAND_Tx_Mess, can_rx);
				CANSPY_DEBUG_COUNT_Fwrd[can_s - 1]++;
#ifdef CANSPY_DEBUG_FILTER
				CANSPY_SCHED_SIGNAL(FWRD_CAN + can_d);
#endif
				break;			
		}
		
		if(silent_fwrd[flag - 1]){
			
			if(canspy_can_variables(can_s, NULL, NULL, NULL, NULL, &pCANS_Hdl, NULL) != EXIT_SUCCESS){
			
				CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Variable);
				return EXIT_FAILURE;
			}

			canspy_can_silence(pCANS_Hdl, false);
		}
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  CAN1 wrapper for the filter service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval None
  */
int canspy_can_filter1(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){
	
	return canspy_can_filter(CAN1_IF, CAN2_IF, flag, opt);
}

/**
  * @brief  CAN2 wrapper for the filter service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval None
  */
int canspy_can_filter2(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){
	
	return canspy_can_filter(CAN2_IF, CAN1_IF, flag, opt);
}

/**
  * @brief  The forward service.
  * @param  can_s: the CAN source
  * @param  can_d: the CAN destination
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_can_forward(uint8_t can_s, uint8_t can_d, CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){
	
	CANSPY_SERVICE_FLAG CAND_Rx_Flag;
	CanTxMsgTypeDef *pCAND_Tx_Mess;
	CAN_HandleTypeDef *pCANS_Hdl;
	CAN_HandleTypeDef *pCAND_Hdl;
	CanRxMsgTypeDef *can_rx;
	
	if(flag == SVC_STOP){

		if(canspy_can_variables(can_s, NULL, NULL, NULL, NULL, &pCANS_Hdl, NULL) != EXIT_SUCCESS){
			
			CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Variable);
			return EXIT_FAILURE;
		}

		canspy_can_silence(pCANS_Hdl, false);
	}
	else if(flag == can_s){
	
		if(canspy_can_variables(can_d, NULL, &CAND_Rx_Flag, NULL, &pCAND_Tx_Mess, &pCAND_Hdl, NULL) != EXIT_SUCCESS){
			
			CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Variable);
			return EXIT_FAILURE;
		}
			
		if((can_rx = canspy_sched_buffer(flag)) == NULL){
			
			CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Buffer);
			return EXIT_FAILURE;
		}
		
		canspy_can_send(ASYNC, (CANSPY_DEVICE_ID)CAND_Rx_Flag, pCAND_Hdl, pCAND_Tx_Mess, can_rx);
		
		if(silent_fwrd[flag - 1]){
			
			if(canspy_can_variables(can_s, NULL, NULL, NULL, NULL, &pCANS_Hdl, NULL) != EXIT_SUCCESS){
			
				CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Variable);
				return EXIT_FAILURE;
			}

			canspy_can_silence(pCANS_Hdl, false);
		}

		CANSPY_DEBUG_COUNT_Fwrd[can_s - 1]++;
		CANSPY_CAN_Match[can_s - 1] = ACT_FWRD;
#ifdef CANSPY_DEBUG_FILTER
		CANSPY_SCHED_SIGNAL(FWRD_CAN + can_d);
#endif
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  CAN1 wrapper for the forward service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval None
  */
int canspy_can_forward1(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){

	return canspy_can_forward(CAN1_IF, CAN2_IF, flag, opt);
}

/**
  * @brief  CAN2 wrapper for the forward service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter..
  * @retval None
  */
int canspy_can_forward2(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){

	return canspy_can_forward(CAN2_IF, CAN1_IF, flag, opt);
}

/**
  * @brief  The inject service.
  * @param  can_s: the CAN source
  * @param  can_d: the CAN destination
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_can_inject(uint8_t can_s, uint8_t can_d, CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){
	
	CANSPY_SERVICE_FLAG CAND_Rx_Flag;
	CanRxMsgTypeDef Temp_Rx_Mess;
	CanTxMsgTypeDef *pCAND_Tx_Mess;
	CAN_HandleTypeDef *pCAND_Hdl;
		
	if(flag == SVC_INIT){
		
		CANSPY_INJ_Services[can_s] = opt->svc_ptr;
	}
	else if(flag == INJ_ETH && (CANSPY_INJ_ETH_If == can_d || CANSPY_INJ_ETH_If == CANA_IF)){
		
		if(canspy_can_variables(can_d, NULL, &CAND_Rx_Flag, NULL, &pCAND_Tx_Mess, &pCAND_Hdl, NULL) != EXIT_SUCCESS){

			CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Variable);
			return EXIT_FAILURE;
		}

		canspy_can_unwrap(&Temp_Rx_Mess, &CANSPY_INJ_ETH_Sock, CANSPY_INJ_ETH_Async);
		canspy_can_send(SYNC, (CANSPY_DEVICE_ID)CAND_Rx_Flag, pCAND_Hdl, pCAND_Tx_Mess, &Temp_Rx_Mess);
		CANSPY_DEBUG_COUNT_Inject[can_d - 1]++;
		
		if(CANSPY_INJ_ETH_Ack == ACK_ECHO)
			canspy_eth_send(ETH_Tx_Buf[0], &CANSPY_INJ_ETH_Sock, sizeof(CANSPY_INJ_ETH_Sock), CANSPY_ETH_Mac[can_d], CANSPY_ETH_Mac[ETH_BROADCAST], CANSPY_ETH_Type[can_d], false);
		else if(CANSPY_INJ_ETH_Ack == ACK_CMD)
			canspy_eth_send(ETH_Tx_Buf[0], "ACK", 3, CANSPY_ETH_Mac[ETH_IF], CANSPY_ETH_Mac[ETH_BROADCAST], CANSPY_ETH_Type[ETH_IF], false);
	}
	else if(flag == INJ_SVC && (CANSPY_INJ_SVC_If == can_d || CANSPY_INJ_SVC_If == CANA_IF)){
		
		if(canspy_can_variables(can_d, NULL, &CAND_Rx_Flag, NULL, &pCAND_Tx_Mess, &pCAND_Hdl, NULL) != EXIT_SUCCESS){

			CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Variable);
			return EXIT_FAILURE;
		}
		
		canspy_can_unwrap(&Temp_Rx_Mess, &CANSPY_INJ_SVC_Sock, CANSPY_INJ_SVC_Async);
		canspy_can_send(SYNC, (CANSPY_DEVICE_ID)CAND_Rx_Flag, pCAND_Hdl, pCAND_Tx_Mess, &Temp_Rx_Mess);
		CANSPY_DEBUG_COUNT_Inject[can_d - 1]++;
	}
	else if(flag == INJ_UART && (CANSPY_INJ_UART_If == can_d || CANSPY_INJ_UART_If == CANA_IF)){
		
		if(canspy_can_variables(can_d, NULL, &CAND_Rx_Flag, NULL, &pCAND_Tx_Mess, &pCAND_Hdl, NULL) != EXIT_SUCCESS){

			CANSPY_DEBUG_ERROR_0(ERROR, CANSPY_CAN_ERROR_Variable);
			return EXIT_FAILURE;
		}

		canspy_can_unwrap(&Temp_Rx_Mess, &CANSPY_INJ_UART_Sock, CANSPY_INJ_UART_Async);
		canspy_can_send(SYNC, (CANSPY_DEVICE_ID)CAND_Rx_Flag, pCAND_Hdl, pCAND_Tx_Mess, &Temp_Rx_Mess);
		CANSPY_DEBUG_COUNT_Inject[can_d - 1]++;
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  CAN1 wrapper for the inject service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval None
  */
int canspy_can_inject1(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){

	return canspy_can_inject(0, CAN1_IF, flag, opt);
}

/**
  * @brief  CAN2 wrapper for the inject service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval None
  */
int canspy_can_inject2(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){

	return canspy_can_inject(0, CAN2_IF, flag, opt);
}
