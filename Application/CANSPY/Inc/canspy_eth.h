/**
  ******************************************************************************
  * @file    canspy_eth.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          ETH module core.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 
 
#ifndef __CANSPY_ETH_H_
#define __CANSPY_ETH_H_

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "stm32f4xx_eth.h"
#include "canspy_sched.h"

/* Exported typedefs ---------------------------------------------------------*/
typedef enum _CANSPY_ETH_ACK{
	
	ACK_NONE,
	ACK_ECHO,
	ACK_CMD,
}
CANSPY_ETH_ACK;

/* Exported constants --------------------------------------------------------*/
#define CANSPY_ETH_HEADER_SIZE ((sizeof(CANSPY_ETH_Mac[0]) * 2) + sizeof(CANSPY_ETH_Type[0]))
#define ETH_BROADCAST 0
#define ETH_IF				3
#define canspy_eth_handler ETH_Rx_Handler
#define ntohl(v)	(uint32_t)(__rev(v))
#define ntohs(v) 	(uint16_t)(__rev(v) >> 16)
#define htonl(v) 	ntohl(v)
#define htons(v) 	ntohs(v)

/* Exported globals ----------------------------------------------------------*/
extern char *CANSPY_ETH_ACK_String[];
extern uint16_t CANSPY_ETH_Type[ETH_IF + 1];
extern uint8_t CANSPY_ETH_Mac[ETH_IF + 1][6];
extern bool CANSPY_ETH_CMD_Output;

/* Exported functions --------------------------------------------------------*/
int canspy_eth_device(CANSPY_DEVICE_CALL call, CANSPY_OPTION_CALL *opt);
uint16_t canspy_eth_short(uint16_t val, bool reverse);
int canspy_eth_send(void *buffer, const void *data, unsigned int size, uint8_t *eth_mac_can, uint8_t *eth_mac_dst, uint16_t type, bool forge);
uint16_t canspy_eth_recv(void *buffer, void *data, unsigned int size, uint8_t *eth_mac_dst, uint8_t *eth_mac_src);
int canspy_eth_bridge(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);
int canspy_eth_wiretap(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);
int canspy_eth_command(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);

#endif /*_CANSPY_ETH_H*/
