/**
  ******************************************************************************
  * @file    canspy_uart.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          UART module core.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 
 
#ifndef __CANSPY_UART_H_
#define __CANSPY_UART_H_

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "stm32f4xx_usart.h"
#include "canspy_sched.h"

/* Exported constants --------------------------------------------------------*/
#define CHAR_LF					'\n'
#define CHAR_CR					'\r'
#define CHAR_SPACE			' '
#define CHAR_DEL				0x7F
#define CHAR_BS					0x08
#define CHAR_ESC				0x1B
#define CHAR_ARROW			0x5B
#define CHAR_LEFT				0x44
#define CHAR_RIGHT			0x43
#define CHAR_DOWN				0x42
#define CHAR_UP					0x41
#define CHAR_PAGE_UP		0x35
#define CHAR_PAGE_DOWN	0x36
#define canspy_uart_handler UART_Rx_Handler

/* Exported macros -----------------------------------------------------------*/
#define CANSPY_UART_STATIC_STRING(s) canspy_print_string(CANSPY_HANDLE_UART, (s), sizeof(s) - 1)
#define CANSPY_UART_STATIC_LINE(s) canspy_print_line(CANSPY_HANDLE_UART, (s), sizeof(s) - 1)
#define CANSPY_UART_BREAK() CANSPY_UART_STATIC_STRING(CANSPY_PRINT_BREAK)

/* Exported globals ----------------------------------------------------------*/
extern uint8_t CANSPY_UART_Rx_Buf[UART_LINE_RX_BUF_SIZE];
extern uint8_t CANSPY_UART_Tx_Buf[UART_LINE_TX_BUF_SIZE];

/* Exported functions --------------------------------------------------------*/
int canspy_uart_device(CANSPY_DEVICE_CALL call, CANSPY_OPTION_CALL *opt);
void canspy_uart_queue(uint8_t *buffer, int length);
int canspy_uart_print(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);
int canspy_uart_monitor(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);
int canspy_uart_viewing(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);
int canspy_uart_shell(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);

#endif /*_CANSPY_UART_H*/
