/**
  ******************************************************************************
  * @file    canspy_shell.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          Shell module extension.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 
 
#ifndef __CANSPY_SHELL_H_
#define __CANSPY_SHELL_H_

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>
#include "canspy_sched.h"

/* Exported typedefs ---------------------------------------------------------*/
typedef void (*CANSPY_SHELL_PROC)(int, char **);

typedef struct _CANSPY_SHELL_CMD{

	const char *name;
	const char *desc;
	const char *help;
	CANSPY_SHELL_PROC address;
	CANSPY_DEVICE_ID dev_id;
}
CANSPY_SHELL_CMD;

typedef struct _CANSPY_SHELL_LIST{

	int size;
	CANSPY_SHELL_CMD *list;
}
CANSPY_SHELL_LIST;

typedef struct _CANSPY_SHELL_PARAM{

	char *name;
	uint16_t *pvariable;
	uint16_t (*translator)(uint16_t, bool);
}
CANSPY_SHELL_PARAM;

/* Exported constants --------------------------------------------------------*/
#define CANSPY_SHELL_HISTORY 32
#define CANSPY_SHELL_PROMPT "#"

/* Exported globals ----------------------------------------------------------*/
extern char CANSPY_SHELL_HISTORY_Buf[CANSPY_SHELL_HISTORY][UART_LINE_RX_BUF_SIZE];
extern int CANSPY_SHELL_HISTORY_Ptr;
extern CANSPY_SHELL_LIST CANSPY_SHELL_Commands;

/* Exported functions --------------------------------------------------------*/
int canspy_shell_register(CANSPY_SHELL_PROC address, const char *name, const char *desc, const char *help, CANSPY_DEVICE_ID dev_id);
int canspy_shell_parse(char *command, int *p_argc, char **p_argv);
int canspy_shell_execute(char *command);
int canspy_shell_prompt(void);

#endif /*_CANSPY_SHELL_H*/
