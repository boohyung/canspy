/**
  ******************************************************************************
  * @file    canspy_service.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          Service module core.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#ifndef __CANSPY_SERVICE_H
#define __CANSPY_SERVICE_H

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "stm32f4xx_can.h"
#include "stm32f4xx_eth.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx_bsp_driver_sd.h"
#include "canspy_option.h"
#include "canspy_debug.h"
 
/* External typedefs ---------------------------------------------------------*/
typedef enum _CANSPY_DEVICE_ID CANSPY_DEVICE_ID;

/* Exported typedefs ---------------------------------------------------------*/
typedef enum _CANSPY_SERVICE_FLAG{

	SVC_SET = OPT_SET,
	SVC_GET = OPT_GET,
	SVC_STOP = OPT_STOP,
	SVC_START = OPT_START,
	SVC_POST = OPT_POST,
	SVC_INIT = OPT_INIT,
	CAN1_RX = CAN1_IF,
	CAN2_RX = CAN2_IF,
	UART_TX,
	UART_RX,
	UART_BREAK,
	INJ_ETH,
	INJ_SVC,
	INJ_UART,
#ifdef CANSPY_DEBUG_FILTER
	ALTR_CAN1,
	ALTR_CAN2,
	DROP_CAN1,
	DROP_CAN2,
	FWRD_CAN1,
	FWRD_CAN2,
#endif
	ETH_RX,
	CANSPY_FLAGS
}
CANSPY_SERVICE_FLAG;

typedef int (*CANSPY_SERVICE_FUNC)(CANSPY_SERVICE_FLAG, CANSPY_OPTION_CALL *);
	
typedef struct _CANSPY_SERVICE_DESC{

	int svc_id;
	CANSPY_OPTION_DESC *svc_opt;
	CANSPY_SERVICE_FUNC svc_func;
	const char *svc_name;
	const char *svc_desc;
	bool exclusive;
	bool asynchrone;
	bool started;
	CANSPY_DEVICE_DESC *dev_ptr;
}
CANSPY_SERVICE_DESC;

/* Exported constants --------------------------------------------------------*/
#define CANSPY_FLAG_FIRST ((CANSPY_SERVICE_FLAG)CAN1_RX)
#ifdef CANSPY_DEBUG_FILTER
	#define FWRD_CAN (FWRD_CAN1 - 1)
	#define DROP_CAN (DROP_CAN1 - 1)
	#define ALTR_CAN (ALTR_CAN1 - 1)
	#define FWRD_FLAG(f) (flag - FWRD_CAN)
	#define DROP_FLAG(f) (flag - DROP_CAN)
	#define ALTR_FLAG(f) (flag - ALTR_CAN)
#endif

/* Exported macros -----------------------------------------------------------*/
#define CANSPY_SERVICE_OPTION(NAME, RANGE, SIZE, ADDR, DESC, RESTART) canspy_option_register(opt->svc_ptr->svc_opt, NAME, ADDR, RANGE, DESC, SIZE, RESTART)
#define CANSPY_SERVICE_ACTIVE() (canspy_service_find(opt->svc_ptr->dev_ptr->dev_id, NULL, NULL)->svc_func == opt->svc_ptr->svc_func)
#define CANSPY_SERVICE_NOMORE() canspy_service_stop(NULL, opt->svc_ptr->dev_ptr->dev_id, NULL, opt->svc_ptr->svc_func)
#define CANSPY_SERVICE_RESTART() {opt->svc_ptr->svc_func(SVC_STOP, opt); opt->svc_ptr->svc_func(SVC_START, opt); opt->svc_ptr->svc_func(flag, opt);}

/* Exported functions --------------------------------------------------------*/
int canspy_service_register(CANSPY_DEVICE_ID dev_id, CANSPY_SERVICE_FUNC func, const char *svc_name, const char *svc_desc, bool exclu, bool async, bool start);
int canspy_service_start(CANSPY_SERVICE_DESC *svc_ptr, CANSPY_DEVICE_ID dev_id, const char *svc_name, CANSPY_SERVICE_FUNC svc_func);
int canspy_service_stop(CANSPY_SERVICE_DESC *svc_ptr, CANSPY_DEVICE_ID dev_id, const char *svc_name, CANSPY_SERVICE_FUNC svc_func);
CANSPY_SERVICE_DESC *canspy_service_walk(CANSPY_DEVICE_DESC *dev_ptr, CANSPY_SERVICE_DESC *cur_svc);
CANSPY_SERVICE_DESC *canspy_service_find(CANSPY_DEVICE_ID dev_id, const char *svc_name, CANSPY_SERVICE_FUNC svc_func);

#endif /*_CANSPY_SERVICE_H*/
